/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.management.admserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.logging.*;
import com.netscape.management.admserv.config.*;
import com.netscape.management.admserv.logging.*;
import netscape.ldap.*;

import com.netscape.management.client.security.CertificateDialog;
import com.netscape.management.client.security.PKCSConfigDialog;
import com.netscape.management.client.security.CertMigrateWizard;

public class AdminResourceModel extends ResourceModel implements IMenuInfo {
    static String iconSource = "com/netscape/management/client/images/";
    static ResourceSet _resource = new ResourceSet("com.netscape.management.admserv.admserv");
    IResourceObject[]_selection;

    static public String MENU_CERT_DIALOG   = "CERTIFICATE SETUP WIZARD";
    static public String MENU_PKCS11_CONFIG = "CERTIFICATE MANAGEMENT";
    static public String MENU_IMPORT_CERT   = "IMPORT CERTIFICATE";
    
    ConsoleInfo _consoleInfo;

    /**
      * Returns supported menu categories
      */
    public String[] getMenuCategoryIDs() {
        return new String[]{ Framework.MENU_FILE//,
            //ResourcePage.MENU_CONTEXT
        };
    }


    /**
      * constrcutor and initialize all the internal variables
      */
    public AdminResourceModel(ConsoleInfo ci, String[] taskList) {


        _consoleInfo = ci;

        ResourceObject root = new AdminServerNode(ci, taskList);

        root.add(new LoggingNode(ci, taskList));

        setRoot(root);
    }


    public IMenuItem[] getMenuItems(String categoryID) {

        ResourceSet _resource = new ResourceSet("com.netscape.management.client.default");
        if (AdminFrameworkInitializer.canAccessSecurity && categoryID.equals(Framework.MENU_FILE)) {
	    MenuItemCategory category = new MenuItemCategory("security", _resource.getString("menu", "FileSecurity"));
	    category.add(new MenuItemText(MENU_CERT_DIALOG, 
			      _resource.getString("menu", "FileManageCert"), 
			      "TODO:description", 
			      new SecurityAction(MENU_CERT_DIALOG)));
	    category.add(new MenuItemText(MENU_PKCS11_CONFIG, 
			      _resource.getString("menu", "FilePKCS11Config"), 
			      "TODO:description", 
			      new SecurityAction(MENU_PKCS11_CONFIG)));
	    category.add(new MenuItemText(MENU_IMPORT_CERT, 
			     _resource.getString("menu", "FileImportCert"),  
			     "TODO:description", 
			     new SecurityAction(MENU_IMPORT_CERT)));
            return new IMenuItem[]{
		category,
                new MenuItemSeparator()
	    };
        }
        return null;
    }

    class SecurityAction implements ActionListener {
	String _id;
	public SecurityAction(String id) {
	    _id = id;
	}
	public void actionPerformed(ActionEvent e) {
	    if (_id.equals(MENU_CERT_DIALOG)) {
                JFrame f = UtilConsoleGlobals.getActivatedFrame();
                try {
                    if (f != null && f instanceof Framework){
                        ((Framework)f).setBusyCursor(true);
                    }
                    CertificateDialog certDialog = new CertificateDialog(f, _consoleInfo, (String)_consoleInfo.get("SIE"));
                    certDialog.setVisible(true);
                }
                finally {
                    if (f != null && f instanceof Framework){
                        ((Framework)f).setBusyCursor(false);
                    }
                }
	    } else if (_id.equals(MENU_PKCS11_CONFIG)) {
                JFrame f = UtilConsoleGlobals.getActivatedFrame();
                try {
                    if (f != null && f instanceof Framework){
                        ((Framework)f).setBusyCursor(true);
                    }
                    PKCSConfigDialog pkcsConfig = new PKCSConfigDialog(f, _consoleInfo, (String)_consoleInfo.get("SIE"));
                    pkcsConfig.setVisible(true);
                }
                finally {
                    if (f != null && f instanceof Framework){
                        ((Framework)f).setBusyCursor(false);
                    }
                }
	    } else if (_id.equals(MENU_IMPORT_CERT)) {
		CertMigrateWizard certMigrate = new CertMigrateWizard(null, _consoleInfo, (String)_consoleInfo.get("SIE"));
		certMigrate.setVisible(true);
	    }
	}
    }

    public void actionObjectSelected(IPage viewInstance,
            IResourceObject[] selection,
            IResourceObject[] previousSelection) {
        super.actionObjectSelected(viewInstance, selection,
                previousSelection);
        _selection = selection;
    }

    /**
     * Notification that a menu item has been selected.
     */
    public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
    }

}
