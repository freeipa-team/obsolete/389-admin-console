/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import com.netscape.management.admserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.logging.*;
import com.netscape.management.admserv.config.*;
import com.netscape.management.admserv.logging.*;
import netscape.ldap.*;

public class LoggingNode extends ResourceObject implements IMenuInfo {

    static ResourceSet _resource = new ResourceSet("com.netscape.management.admserv.admserv");
    static RemoteImage _logFolderIcon =
            new RemoteImage(_resource.getString("log", "FolderIcon"));
    static RemoteImage _logFileIcon =
            new RemoteImage(_resource.getString("log", "FileIcon"));

    private ConsoleInfo _ci;

    private BaseConfigPanel _configPanel;

    static public String MENU_CONFIG = "CONFIG";

    private boolean _menuConfigEnabled;

    public LoggingNode(ConsoleInfo ci, String[] taskList) {
        _ci = ci;

        //If a task is not visible to the user, then it should not be accessable
        //from the resorce tree
        _menuConfigEnabled = findTask("task.Logging", taskList);

        setName(_resource.getString("resourcepage","Logs"));
        setIcon(_logFolderIcon);

        setAllowsChildren(true); // folder
        add(new AccessLogNode(ci));
        add(new ErrorLogNode(ci));

    }

    private boolean findTask(String task, String[] list) {
        for (int i = 0; i < list.length; i++) {
            if (list[i].endsWith(task))
                return true;
        }
        return false;
    }

    public String[] getMenuCategoryIDs() {
        return new String[]{ ResourcePage.MENU_OBJECT,
        ResourcePage.MENU_CONTEXT };
    }

    public IMenuItem[] getMenuItems(String categoryID) {
        if ((categoryID.equals(ResourcePage.MENU_OBJECT)) ||
                (categoryID.equals(ResourcePage.MENU_CONTEXT))) {
            String i18nMenuConfig = _resource.getString("menu", "Config");
            String i18nDescrMenuConfig =
                    _resource.getString("menuDescription", "ConfigAdmin");

            return new IMenuItem[]{ new MenuItemSeparator(),
            new MenuItemText(MENU_CONFIG, i18nMenuConfig,
                    i18nDescrMenuConfig, _menuConfigEnabled)};
        }
        return null;
    }

    public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
        if (viewInstance instanceof ResourcePage) {
            if (item.getID().equals(MENU_CONFIG)) {
                ResourcePage page = (ResourcePage) viewInstance;
                _configPanel = null;
                page.setCustomPanel(getCustomPanel());
            } else {
                Debug.println("Not Yet Implemented: " + item);
            }
        }
    }


    public Component getCustomPanel() {
        if (!_menuConfigEnabled)
            return null;

        if (_configPanel == null || !_configPanel.isInitialized()) {
            _configPanel =
                    new BaseConfigPanel(new LoggingConfigPanel("", _ci));
        }
        return _configPanel;
    }

}
class AccessLogNode extends ResourceObject {
    ConsoleInfo _ci;

    public AccessLogNode(ConsoleInfo ci) {
        _ci = ci;
        setName(LoggingNode._resource.getString("resourcepage","LogAccess"));
        setIcon(LoggingNode._logFileIcon);
    }

    public Component getCustomPanel() {
        return new LogViewer(new AccessLogViewerModel(_ci, "admin-serv/tasks/Configuration/ReadLog"));
    }
}

class ErrorLogNode extends ResourceObject {
    ConsoleInfo _ci;

    public ErrorLogNode(ConsoleInfo ci) {
        _ci = ci;
        setName(LoggingNode._resource.getString("resourcepage","LogError"));
        setIcon(LoggingNode._logFileIcon);
    }

    public Component getCustomPanel() {
        return new LogViewer(new ErrorLogViewerModel(_ci, "admin-serv/tasks/Configuration/ReadLog"));
    }
}
