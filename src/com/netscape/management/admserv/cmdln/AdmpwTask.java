/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.cmdln;


import java.util.Vector;
import java.net.URL;
import com.netscape.management.client.util.AdmTask;
import com.netscape.management.client.util.AdmTaskArg;
import com.netscape.management.client.util.Debug;


/**
 * Utility class to execute the count log entries task.
 *
 * @author   Peter Lee
 * @version  %I%, %G%
 * @see      AdmTask
 */

public class AdmpwTask extends AdmTask {

    private int _status;

    /**
     * Constructor to build a task url.
     *
     * @param url           the task url
     * @param userID        either username or user's DN String
     * @param userPassword  password of the users
     */
    public AdmpwTask(URL url, String userID, String userPassword) {

        super(url, userID, userPassword);
        _status = 0; // Default to success.
    }


    /**
      * Constructor to build a task url.
      *
      * @param admProtocol   protocol used by the target Admin Server (http/https)
      * @param admServ       host name of the target Admin Server
      * @param admPort       port number of the target Admin Server
      * @param serverID      name of the server which task will apply to
      * @param taskID        task name to be executed
      * @param args          arguments for the task
      * @param userID        either username or user's DN String
      * @param userPassword  password of the user
      */
    public AdmpwTask(String admProtocol, String admServ, int admPort,
            String serverID, String taskID, Vector args,
            String userID, String userPassword) {

        super(admProtocol, admServ, admPort, serverID, taskID, args,
                userID, userPassword);
        _status = 0; // Default to success.
    }


    /**
      * Parsing specific to the tasks involving the admpw CGI.
      *
      * @param s  the input data line to be parsed
      */
    public void parse(String s) {
        //Debug.println("TRACE AdmpwTask.parse: " + s);

        int index;
        if ((index = s.indexOf(":")) != (-1)) {
            // This implies an error occurred. Look at the ReadLog.c CGI.
            String name = s.substring(0, index).trim();
            String value = s.substring(index + 1).trim();
            if (name.equalsIgnoreCase("NMC_Status")) {
                _status = Integer.parseInt(value);
                addResponseArgument(new AdmTaskArg("NMC_ErrInfo", "Internal Error"));
            } else {
                addResponseArgument(new AdmTaskArg(name, value));
            }
        } else {
            _status = 1;
            addResponseArgument(new AdmTaskArg("NMC_ErrInfo", "Internal Error"));
        }
    }


    /**
      * Status specific to the tasks involving the admpw CGI.
      * Need to return superclass's getStatus in case exec failed!
      *
      * @return  the integer result of running the task
      */
    public int getStatus() {
        if (_status != 0) {
            return _status;
        } else {
            return super.getStatus();
        }
    }
}
