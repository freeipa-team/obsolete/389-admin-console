/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.management.client.*;
import com.netscape.management.admserv.panel.*;
import com.netscape.management.admserv.task.AdminTaskObject;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;


/**
 *	Initializes Admin Server Config framework
 */
public class AdminFrameworkInitializer extends FrameworkInitializer {
    private static String PERMID_SECURITY = "SecurityVisibility";
    public static boolean canAccessSecurity = false;

    public AdminFrameworkInitializer(ConsoleInfo consoleInfo) {
        Console.setConsoleInfo(consoleInfo); // workaround to classloading static reference problem
        UIPermissions uip = new UIPermissions(LDAPUtil.getAdminGlobalParameterEntry());
        canAccessSecurity = uip.hasPermission(PERMID_SECURITY);
        TaskPage taskPage = new TaskPage(consoleInfo);
        TaskObject root = (TaskObject) taskPage.getModel().getRoot();

        addPage(taskPage);
        String [] taskList = getTaskList(taskPage);
        setTaskConsoleInfo(taskPage, consoleInfo); // override cloning of ConsoleInfo
        addPage( new AdminResourcePage(
                new AdminResourceModel(consoleInfo, taskList)));
        RemoteImage imageIcon = new RemoteImage(
                AdminServer._resource.getString("admin", "largeIcon"));
        setMinimizedImage(imageIcon.getImage());

        setBannerImage(getBannerImage(consoleInfo));
        setFrameTitle(consoleInfo.getLDAPConnection(),
                consoleInfo.getCurrentDN());
        setBannerText("");
    }

    private Image getBannerImage(ConsoleInfo consoleInfo) {
        String imageBase="com/netscape/management/admserv/images/";
        return new RemoteImage(imageBase + "admin.gif").getImage();
    }

    private String[] getTaskList(TaskPage taskPage) {
        TaskObject root = (TaskObject) taskPage.getModel().getRoot();
        String[] tasks = new String[root.getChildCount()];
        for (int i = 0; i < root.getChildCount(); i++) {
            tasks[i] = root.getChildAt(i).getClass().getName();
        }
        return tasks;
    }

    /**
      * TaskPage clones ConsoleInfo when creating a task. We want to
      * override that and force all Admin Server tasks to use the same
      * adminURL as the AdminServer object. This is important as
      * adminURL might change if the port number is changed.
      */
    private void setTaskConsoleInfo(TaskPage taskPage,
            ConsoleInfo consoleInfo) {
        TaskObject root = (TaskObject) taskPage.getModel().getRoot();
        for (int i = 0; i < root.getChildCount(); i++) {
            AdminTaskObject task = (AdminTaskObject) root.getChildAt(i);
            task.setConsoleInfo( new TaskConsoleInfo(task.getConsoleInfo(),
                    consoleInfo));
        }
    }

    /**
      * TaskConsoleInfo class overrides getAdminURL to return the
      * value from the ConsoleInfo in the ServerNode
      */
    private static class TaskConsoleInfo extends ConsoleInfo {
        private ConsoleInfo serverNodeConsoleInfo;
        public TaskConsoleInfo(ConsoleInfo taskConsoleInfo,
                ConsoleInfo serverNodeConsoleInfo) {
            for (Enumeration eKey = taskConsoleInfo.keys();
                    eKey.hasMoreElements();) {
                Object oKey = eKey.nextElement();
                Object oValue = taskConsoleInfo.get(oKey);
                put(oKey, oValue);
            }
            this.serverNodeConsoleInfo = serverNodeConsoleInfo;
        }
        public String getAdminURL() {
            return serverNodeConsoleInfo.getAdminURL();
        }
        /* We have to make sure we use the same password throughout
         * for the main console and for the tasks - if we do not do
         * this and we change the password, the tasks will use the
         * wrong password.  This way, if we change the password from
         * either the Configure task, or from the Configuration tab,
         * we will use the same password throughout.
         */
        public void setAuthenticationPassword(String password) {
            serverNodeConsoleInfo.setAuthenticationPassword(password);
        }
        public String getAuthenticationPassword() {
        	return serverNodeConsoleInfo.getAuthenticationPassword();
        }
    };

    private void removeTask(TaskObject root, String taskName) {
        for (int i = 0; i < root.getChildCount(); i++) {
            // can not use instanceof because of compilation dependency
            if (root.getChildAt(i).getClass().getName().endsWith(
                    taskName)) {
                ((TaskObject) root).remove(i);
                break;
            }
        }
    }
}


class AdminResourcePage extends ResourcePage {
    public AdminResourcePage(IResourceModel model) {
        super(model);
    }

    public void initialize(Framework framework) {
        MenuItemText item = new MenuItemText(
                AdminServer._resource.getString("menu",
                "HelpAdminServer"), "TODO:description");
        item.addActionListener(new HelpAction());
        framework.addMenuItem(Framework.MENU_HELPWEBHELP, item);
    }

    class HelpAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Help helpSession = new Help(AdminServer._resource);
            helpSession.contextHelp("menubar", "adminserverHelpToken");
        }
    }
}
