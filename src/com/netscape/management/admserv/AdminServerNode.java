/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import com.netscape.management.nmclf.*;
import com.netscape.management.admserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.admserv.config.*;
import netscape.ldap.*;


public class AdminServerNode extends ResourceObject implements IMenuInfo {
    public static ResourceSet _resource;
    public static Icon _icon;
    public static String _i18nMenuConfig, _i18nMenuRestart, _i18nMenuStop;
    public static String _i18nDescrMenuConfig, _i18nDescrMenuRestart,
    _i18nDescrMenuStop;
    public static String _i18nStopTask, _i18nAckStop;

    static {
        _resource = new ResourceSet("com.netscape.management.admserv.admserv");
        _icon = new RemoteImage(_resource.getString("admin", "smallIcon"));
        _i18nMenuConfig = _resource.getString("menu", "Config");
        _i18nMenuRestart = _resource.getString("menu", "Restart");
        _i18nMenuStop = _resource.getString("menu", "Stop");
        _i18nDescrMenuConfig = _resource.getString("menuDescription", "ConfigAdmin");
        _i18nDescrMenuRestart =
                _resource.getString("menuDescription", "RestartAdmin");
        _i18nDescrMenuStop = _resource.getString("menuDescription", "StopAdmin");
        _i18nAckStop = _resource.getString("stop", "ack");
        _i18nStopTask = _resource.getString("taskName", "stop");

    }

    static public String MENU_CONFIG = "CONFIG";
    static public String MENU_RESTART = "RESTART";
    static public String MENU_STOP = "STOP";

    private BaseConfigPanel _configPanel;
    private boolean _menuConfigEnabled, _menuRestartEnabled,
    _menuStopEnabled;

    private ConsoleInfo _consoleInfo;

    public AdminServerNode(ConsoleInfo ci, String[] taskList) {
        super(AdminServer._resource.getString("admin","title"));

        _consoleInfo = ci;

        //If a task is not visible to the user, then it should not be accessable
        //from the resorce tree
        _menuConfigEnabled = findTask("task.ServerSetup", taskList);
        _menuRestartEnabled = findTask("task.Restart", taskList);
        _menuStopEnabled = findTask("task.Stop", taskList);

        setAllowsChildren(true);
    }

    private boolean findTask(String task, String[] list) {
        for (int i = 0; i < list.length; i++) {
            if (list[i].endsWith(task))
                return true;
        }
        return false;
    }

    public Icon getIcon() {
        return _icon;
    }

    public String[] getMenuCategoryIDs() {
        return new String[]{ ResourcePage.MENU_OBJECT,
        ResourcePage.MENU_CONTEXT };
    }

    public IMenuItem[] getMenuItems(String categoryID) {
        if ((categoryID.equals(ResourcePage.MENU_OBJECT)) ||
                (categoryID.equals(ResourcePage.MENU_CONTEXT))) {
            return new IMenuItem[]{ new MenuItemSeparator(),
            new MenuItemText(MENU_CONFIG, _i18nMenuConfig,
                    _i18nDescrMenuConfig, _menuConfigEnabled),
            new MenuItemText(MENU_RESTART, _i18nMenuRestart,
                    _i18nDescrMenuRestart, _menuRestartEnabled),
            new MenuItemText(MENU_STOP, _i18nMenuStop,
                    _i18nDescrMenuStop, _menuStopEnabled)};
        }
        return null;
    }

    public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
        if (viewInstance instanceof ResourcePage) {
            if (item.getID().equals(MENU_CONFIG)) {
                ResourcePage page = (ResourcePage) viewInstance;
                _configPanel = null;
                page.setCustomPanel(getCustomPanel());
            } else if (item.getID().equals(MENU_STOP)) {
                ResourcePage page = (ResourcePage) viewInstance;

                int userSelection = SuiOptionPane.showConfirmDialog(page,
                        _i18nAckStop, _i18nStopTask,
                        SuiOptionPane.YES_NO_OPTION);

                if (userSelection != SuiOptionPane.YES_OPTION) {
                    return;
                }

                AdminOperation operation = new StopOperation(_consoleInfo);
                operation.monitorOperation("");
                String title =
                        AdminServer._resource.getString("taskName","stop");
                DialogFrame dialog = new DialogFrame(page, title,
                        operation.getPanel());
                dialog.setVisible(true);
                dialog.dispose();
                ModalDialogUtil.sleep();
            } else if (item.getID().equals(MENU_RESTART)) {
                ResourcePage page = (ResourcePage) viewInstance;

                if (!canRestartFromConsole(page))
                    return;

                AdminOperation operation =
                        new RestartOperation(_consoleInfo);
                operation.monitorOperation("");
                String title =
                        AdminServer._resource.getString("taskName","restart");
                DialogFrame dialog = new DialogFrame(page, title,
                        operation.getPanel());
                dialog.setVisible(true);
                dialog.dispose();
                ModalDialogUtil.sleep();
            } else {
                Debug.println("Not Yet Implemented: " + item);
            }
        }
    }

    private boolean canRestartFromConsole(JPanel parent) {

        IRestartControl restartControl =
                (IRestartControl)_consoleInfo.get(IRestartControl.ID);
        if (restartControl != null) {
            if (!restartControl.canRestartFromConsole()) {
                String msg =
                        AdminServer._resource.getString("restart","canNotRestart");
                SuiOptionPane.showMessageDialog(parent, msg, getName(),
                        SuiOptionPane.ERROR_MESSAGE);
                return false;
            }
        } else {
            Debug.println("RestartOperation: restart activator not in ConsoleInfo");
        }
        return true;
    }


    public Component getCustomPanel() {
        if (!_menuConfigEnabled)
            return null;

        if (_configPanel == null || !_configPanel.isInitialized()) {
            _configPanel = new BaseConfigPanel(
                    new AdminConfigPanel(_consoleInfo));
        }
        return _configPanel;
    }

    /**
         * Called when this object needs to execute, (when user double-clicks or menu Open)
      * @return success or failure of run operation
         * Called by: ResourceModel
      */
    public boolean run(IPage viewInstance, IResourceObject selection[]) {
        return false;
    }
}
