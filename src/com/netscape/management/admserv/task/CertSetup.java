/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.task;

import java.awt.*;
import javax.swing.*;
import com.netscape.management.admserv.*;
import com.netscape.management.client.*;
import com.netscape.management.client.security.CertificateDialog;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import netscape.ldap.*;

/**
  * @author schihm@netscape.com
  * @author adam@netscape.com
  * @author ahakim@netscape.com
  * @see AdminTaskObject
  */
public class CertSetup extends AdminTaskObject {
    public CertSetup() {
        setName(AdminServer._resource.getString("taskName","CertSetup"));
        setDescription(
                AdminServer._resource.getString("taskDescription","CertSetup"));
    }

    /**
      *      Called when user wants to execute this object, invoked by a
      *  double-click gesture or a menu action.  In the case of multiply
      *  selected objects, this method is called once for each object
      *  (in top-bottom order).  A return value of true indicates that a
      *  run action has been initiated, and not to call run() on other
      *  selected objects.
      */
    public boolean run(IPage viewInstance) {
        IFramework framework = viewInstance.getFramework();
        JFrame frame = framework.getJFrame();
        try {
            ((Framework)framework).setBusyCursor(true);
            CertificateDialog certDialog = new CertificateDialog(frame, _consoleInfo, (String)(_consoleInfo.get("SIE")));
            certDialog.show();
        }
        finally {
            ((Framework)framework).setBusyCursor(false);
        }
        return true;
    }
}
