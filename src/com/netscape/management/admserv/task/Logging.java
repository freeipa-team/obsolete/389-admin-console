/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.task;

import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.management.admserv.*;
import com.netscape.management.admserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.admserv.config.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;

/**
  *
  * @version 0.1 11/01/97
  * @author ahakim@netscape.com
  * @see AdminTaskObject
  */
public class Logging extends AdminTaskObject {
    public Logging() {
        setName(AdminServer._resource.getString("taskName","logging"));
        setDescription(
                AdminServer._resource.getString("taskDescription","logging"));
    }

    public boolean run(IPage viewInstance) {
        IFramework framework = viewInstance.getFramework();
        JFrame frame = framework.getJFrame();

        JDialog dialog = getConfigDialog(frame);
        dialog.setSize(515, 425);
        dialog.setVisible(true);
        dialog.dispose();
        ModalDialogUtil.sleep();
        return true;
    }

    public JDialog getConfigDialog(Component parent) {

        DialogFrame configDialog = null;
        configDialog = new DialogFrame(parent, getName(),
                new BaseConfigPanel(
                new LoggingConfigPanel("", _consoleInfo),
                /*inDialog=*/true));
        return configDialog;
    }
}
