/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.task;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import com.netscape.management.admserv.*;
import com.netscape.management.client.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.acleditor.*;

/**
  * @version 0.1 11/01/97
  * @author ahakim@netscape.com
  * @see ITaskObject
  * @see IResourceObject
  */
public class AdminTaskObject extends TaskObject implements IResourceObject,
IMenuInfo {
    static ResourceSet _resource = new ResourceSet("com.netscape.management.client.default");
    static Icon _icon = new RemoteImage("com/netscape/management/client/images/task.gif");
    static String MENU_SET_ACL = "setAcl";


    public Icon getIcon() {
        return _icon;
    }

    public Icon getLargeIcon()// NOT SUPPORTED in this case
    {
        return null;
    }

    public void setIcon(Icon icon) {
        _icon = icon;
    }

    public Component getCustomPanel() {
        return (Component) null;
    }

    public String[] getMenuCategoryIDs() {
        return new String[]{ Framework.MENU_FILE,
        ResourcePage.MENU_CONTEXT };
    }

    /**
      * add menu items for this page.
      */
    public IMenuItem[] getMenuItems(String category) {
        // for all categories, do the same thing...
        return new IMenuItem[]{ new MenuItemText(MENU_SET_ACL,
                _resource.getString("menu", "EditSetACL"), "TODO: description")};
    }

    /**
       * Notification that a menu item has been selected.
       */
    public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
        if (item.getID().equals(MENU_SET_ACL)) {
            ConsoleInfo ci = (ConsoleInfo) getConsoleInfo().clone();
            ci.setAclDN(getConsoleInfo().getCurrentDN());
            ci.setUserGroupDN("");
            ACLEditor ed = new ACLEditor(ci);
            ed.show();
        }
    }

    public boolean run(IPage viewInstance, IResourceObject selection[]) {
        return false;
    }

    public boolean canRunSelection(IResourceObject selection[]) {
        return true;
    }
}
