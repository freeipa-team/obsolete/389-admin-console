/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.task;

import java.awt.*;
import javax.swing.*;
import com.netscape.management.nmclf.*;
import com.netscape.management.admserv.*;
import com.netscape.management.admserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.admserv.config.*;
import com.netscape.management.client.util.*;

/**
  *
  * @version 0.1 11/01/97
  * @author ahakim@netscape.com
  * @see AdminTaskObject
  */
public class Stop extends AdminTaskObject {
    public Stop() {
        setName(AdminServer._resource.getString("taskName","stop"));
        setDescription(
                AdminServer._resource.getString("taskDescription","stop"));
    }

    public boolean run(IPage viewInstance) {
        TaskPage page = (TaskPage) viewInstance;

        String ackMsg = AdminServer._resource.getString("stop","ack");
        int userSelection = SuiOptionPane.showConfirmDialog(page, ackMsg,
                getName(), SuiOptionPane.YES_NO_OPTION);

        if (userSelection != SuiOptionPane.YES_OPTION) {
            return false;
        }

        AdminOperation operation = new StopOperation(_consoleInfo);
        operation.monitorOperation("");
        DialogFrame dialog =
                new DialogFrame(page, getName(), operation.getPanel());
        dialog.setVisible(true);
        dialog.dispose();
        ModalDialogUtil.sleep();
        return true;
    }
}

