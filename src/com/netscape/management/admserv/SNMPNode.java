/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import com.netscape.management.admserv.panel.*;
import com.netscape.management.client.*;
import com.netscape.management.client.console.*;
import com.netscape.management.client.util.*;
import com.netscape.management.admserv.config.*;
import netscape.ldap.*;


public class SNMPNode extends ResourceObject implements IMenuInfo {

    public static ResourceSet _resource;
    public static String _i18nMenuConfig, _i18nMenuRestart, _i18nMenuStop;
    public static String _i18nDescrMenuConfig, _i18nDescrMenuRestart,
    _i18nDescrMenuStop;
    public static String _i18nDialogTitleRestart, _i18nDialogTitleStop,
    _i18nSNMPMasterAgent;

    static {
        _resource = new ResourceSet("com.netscape.management.admserv.admserv");
        _i18nMenuConfig = _resource.getString("menu", "Config");
        _i18nMenuRestart = _resource.getString("menu", "Restart");
        _i18nMenuStop = _resource.getString("menu", "Stop");
        _i18nDescrMenuConfig = _resource.getString("menuDescription", "ConfigSNMP");
        _i18nDescrMenuRestart =
                _resource.getString("menuDescription", "RestartSNMP");
        _i18nDescrMenuStop = _resource.getString("menuDescription", "StopSNMP");
        _i18nDialogTitleStop = _resource.getString("taskDescription", "SNMPStop");
        _i18nDialogTitleRestart =
                _resource.getString("taskDescription", "SNMPRestart");
        _i18nSNMPMasterAgent = _resource.getString("taskName", "SNMPMasterAgent");
    }

    static public String MENU_CONFIG = "CONFIG";
    static public String MENU_RESTART = "RESTART";
    static public String MENU_STOP = "STOP";

    private ConsoleInfo _consoleInfo;
    private BaseConfigPanel _configPanel;
    private boolean _menuConfigEnabled;


    public SNMPNode(ConsoleInfo ci, String[] taskList) {
        super(_i18nSNMPMasterAgent);
        _consoleInfo = ci;

        //If a task is not visible to the user, then it should not be accessable
        //from the resorce tree
        _menuConfigEnabled = findTask("task.SNMPSetup", taskList);

        setAllowsChildren(false);
    }


    private boolean findTask(String task, String[] list) {
        for (int i = 0; i < list.length; i++) {
            if (list[i].endsWith(task))
                return true;
        }
        return false;
    }

    public Icon getIcon() {
        return null;
    }

    public String[] getMenuCategoryIDs() {
        return new String[]{ ResourcePage.MENU_OBJECT,
        ResourcePage.MENU_CONTEXT };
    }

    public IMenuItem[] getMenuItems(String categoryID) {
        return new IMenuItem[]{ new MenuItemSeparator(),
        new MenuItemText(MENU_CONFIG, _i18nMenuConfig,
                _i18nDescrMenuConfig, _menuConfigEnabled),
        new MenuItemText(MENU_RESTART, _i18nMenuRestart,
                _i18nDescrMenuRestart, _menuConfigEnabled),
        new MenuItemText(MENU_STOP, _i18nMenuStop,
                _i18nDescrMenuStop, _menuConfigEnabled)};
    }

    public void actionMenuSelected(IPage viewInstance, IMenuItem item) {
        if (viewInstance instanceof ResourcePage) {
            if (item.getID().equals(MENU_CONFIG)) {
                ResourcePage page = (ResourcePage) viewInstance;
                _configPanel = null;
                page.setCustomPanel(getCustomPanel());
            } else if (item.getID().equals(MENU_STOP)) {
                String taskURL = "admin-serv/tasks/Operation/SNMPControl?ACTION=STOP";
                ResourcePage page = (ResourcePage) viewInstance;
                AdminOperation operation =
                        new AdminOperation(_consoleInfo, taskURL);
                operation.monitorOperation("");
                //page.setCustomPanel(operation.getPanel());
                String title = _i18nDialogTitleStop;
                DialogFrame dialog = new DialogFrame(page, title,
                        operation.getPanel());
                dialog.setVisible(true);
                dialog.dispose();
                ModalDialogUtil.sleep();
            } else if (item.getID().equals(MENU_RESTART)) {
                String taskURL = "admin-serv/tasks/Operation/SNMPControl?ACTION=RESTART";
                ResourcePage page = (ResourcePage) viewInstance;
                AdminOperation operation =
                        new AdminOperation(_consoleInfo, taskURL);
                operation.monitorOperation("");
                //page.setCustomPanel(operation.getPanel());
                String title = _i18nDialogTitleRestart;
                DialogFrame dialog = new DialogFrame(page, title,
                        operation.getPanel());
                dialog.setVisible(true);
                dialog.dispose();
                ModalDialogUtil.sleep();
            } else {
                Debug.println("Not Yet Implemented: " + item);
            }
        }
    }

    /**
      * return the host information panel to the caller
      */
    public Component getCustomPanel() {
        if (!_menuConfigEnabled)
            return null;

        if (_configPanel == null || !_configPanel.isInitialized()) {
            _configPanel = new BaseConfigPanel(
                    new SNMPConfigPanel(_consoleInfo));
        }

        return _configPanel;
    }
}
