/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.config;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.netscape.management.client.util.*;
import com.netscape.management.nmclf.SuiLookAndFeel;

/**
 * Error Dialog for Config Exceptions
*/
public class ConfigInfoDialog {

    static ResourceSet _resource;
    static String  _i18nDefaultTitle;

    static {
        _resource = new ResourceSet("com.netscape.management.admserv.config.config");
        _i18nDefaultTitle = _resource.getString("infodialog", "DefaultTitle");
    }

    private Component _parent;
    private ButtonBar _buttons;
    private Font _fontLabel = UIManager.getFont("Panel.font");

    public static void showDialog(Component parent, String text,
            String title) {
        Frame frame = ConfigErrorDialog.getParentFrame(parent);

        BaseConfigPanel.clearProgressIndicator(parent);

        if (title == null || title.length() == 0) {
            title = _i18nDefaultTitle;
        }

        if (Thread.currentThread().getClass().getName().equals("java.awt.EventDispatchThread")) {
            JOptionPane.showMessageDialog(frame, text, title,
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            final String title_f = title, text_f = text;
            final Frame frame_f = frame;

            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                JOptionPane.showMessageDialog(frame_f,
                                        text_f, title_f,
                                        JOptionPane.INFORMATION_MESSAGE);
                            }
                        }
                        );
            } catch (Exception e1) {
                Debug.println("showDialog invokeAndWait " + e1);
            }
        }

    }
}
