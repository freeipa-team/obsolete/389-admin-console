/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.config;

/**
 * A generic interface for config dialog data model
 */
public interface IConfigDataModel {

    /**
     * Returns true id data model is loaded with server data
    */
    public boolean isLoaded();

    /**
     * Load data model from the server
     * @exception RemoteRequestException
    */
    public void load() throws RemoteRequestException;

    /**
     * Returns true is data model is modified
    */
    public boolean isModified();

    /**
     * Save data model to the server
     * @exception RemoteRequestException
    */
    public void save() throws RemoteRequestException;

    /**
     * Returns enumeration for attributes
     * @return Enumeration for attributes
    */
    public java.util.Enumeration getAttributes();

    /**
     * Get Value for the attribute with the specified name
     * @param attr the name of an attribute
     * @return value for the attribute
    */
    public String getAttribute(String attr);

    /**
     * Set the value for an attribute
     * @param attr the name for an attribute
     * @param val the value for the attribute
    */
    public void setAttribute(String attr,
            String val) throws ValidationException;

    /**
     * Return the model name. This can be the server name.
     * @return the identifier for the data model
    */
    public String getModelName();
}
