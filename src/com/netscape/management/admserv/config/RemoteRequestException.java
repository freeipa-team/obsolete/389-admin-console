/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.config;

import java.net.URL;
import com.netscape.management.client.comm.HttpException;
import netscape.ldap.LDAPException;

public class RemoteRequestException extends ConfigPanelException {

    public static final int HTTP = 0;
    public static final int LDAP = 1;

    private int _type = HTTP;
    private String _url, _dn , _matchedDN;
    private String _message;

    public RemoteRequestException(String url, String message) {
        _url = url;
        _message = message;
        _type = HTTP;
    }

    public RemoteRequestException(HttpException e) {
        _url = e.getURL().toString();
        _message = e.getStatus();
        _type = HTTP;
    }

    public RemoteRequestException(LDAPException e, String dn) {
        _dn = dn;
        _matchedDN = e.getMatchedDN();
        _message = e.toString();
        _type = LDAP;
    }

    public RemoteRequestException(Exception e) {
        if (e instanceof HttpException) {
            _url = ((HttpException) e).getURL().toString();
            _message = ((HttpException) e).getStatus();
        } else {
            _url = "";
            _message = e.getMessage();
        }
    }

    public int getType() {
        return _type;
    }

    public String getURL() {
        return _url;
    }

    public String getDN() {
        return _dn;
    }

    public String getMatchedDN() {
        return _matchedDN;
    }

    public String getMessage() {
        return _message;
    }

    public String toString() {
        return "RemoteRequestException:\n" + "Message: " + _message +
                "\n" + "URL:      " + _url;
    }
}
