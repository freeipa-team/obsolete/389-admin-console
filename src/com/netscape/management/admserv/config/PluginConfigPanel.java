/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.config;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import com.netscape.management.nmclf.SuiLookAndFeel;
import com.netscape.management.client.util.*;

/**
  *
  * @version 0.1 11/28/97
  * @author miodrag@netscape.com
  */

public abstract class PluginConfigPanel extends JPanel implements IPluginConfigPanel {

    private String _titleText = "";

    public PluginConfigPanel() {
        this("");
    }

    public PluginConfigPanel(String title) {
        //setBorder(new EmptyBorder(0,5,0,5));
        _titleText = title;
    }

    public JPanel getPanel() {
        return this;
    }

    public String getTitleText() {
        return _titleText;
    }

    public void showHelp() {
        Debug.println("help not implemented");
    }

}
