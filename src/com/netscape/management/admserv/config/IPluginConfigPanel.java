/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.config;

/**
  *
  * @version 0.1 11/28/97
  * @author miodrag@netscape.com
  */

public interface IPluginConfigPanel {

    /**
     * Returns the title for the panel.
    */
    public String getTitleText();

    /**
     * Returns the reference to the panel
    */
    public javax.swing.JPanel getPanel();

    /**
     * Returns the data model for the panel
    */
    public IConfigDataModel getDataModel();

    /**
     * Set data model for the panel. The panel can swith data model dynamically.
     *
     * @param dataModel A data model for the panel
    */
    public void setDataModel(IConfigDataModel datModel)
            throws ConfigPanelException;

    /**
     * Called to initialize the panel content. This method should be used to
     * load the model if necessary and populate the panel with the model data. The
     * panel layout should be created in the constructor, so that panel can be created
     * before data are loaded
    */
    public void initialize() throws ConfigPanelException;

    /**
     * Register ui components that modify data with the edit monitor. The
     * edit monitor encapsulates 'dirty' flag and enables button bar buttons
     * if data are modifed
    */
    public void registerEditComponents(EditMonitor monitor);

    /**
     * Reset panel content when 'cancel' button is applied.
    */
    abstract public void resetContent() throws ConfigPanelException;

    /**
     * Apply changes when panel is modified and 'ok' button is pressed. The method
     * should just propagate the changes into the data model. The framework will
     * invoke save operation on the model, if the model is modified.
    */
    abstract public void applyChanges() throws ConfigPanelException;

    /**
     * Method called when 'help' button is pressed.
    */
    abstract public void showHelp();

}
