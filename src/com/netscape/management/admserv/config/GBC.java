/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.config;

import java.awt.Insets;
import java.awt.GridBagConstraints;

public class GBC extends java.awt.GridBagConstraints {

    public void setInsets(int top, int left, int bottom, int right) {
        this.insets = new Insets(top, left, bottom, right);
    }

    public void setGrid(int x, int y, int w, int h) {
        this.gridx = x;
        this.gridy = y;
        this.gridwidth = w;
        this.gridheight = h;
    }

    public void setSpace(double weightx, double weighty, int anchor,
            int fill) {
        this.weightx = weightx;
        this.weighty = weighty;
        this.anchor = anchor;
        this.fill = fill;
    }
}

