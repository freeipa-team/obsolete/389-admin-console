/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.config;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import com.netscape.management.nmclf.*;
import com.netscape.management.client.util.*;
import com.netscape.management.nmclf.SuiLookAndFeel;

/**
 * Error Dialog for Config Exceptions
*/
public class ConfigErrorDialog {

    static ResourceSet _resource;
    static String _i18nDefaultTitle, _i18nValidationError, _i18nRemoteRequestError;
    static String _i18nURL, _i18nDataItem, _i18nDN, _i18nMatchedDN;

    static {
        _resource = new ResourceSet("com.netscape.management.admserv.config.config");
        _i18nDefaultTitle = _resource.getString("errdialog", "DefaultTitle");
        _i18nValidationError = _resource.getString("errdialog", "ValidationError");
        _i18nRemoteRequestError = _resource.getString("errdialog", "RemoteRequestError");
        _i18nURL = _resource.getString("errdialog", "URL");
        _i18nDN = _resource.getString("errdialog", "DN");
        _i18nMatchedDN = _resource.getString("errdialog", "MatchedDN");
        _i18nDataItem = _resource.getString("errdialog", "DataItem");
    }


    public static void showDialog(Component parent,
            ConfigPanelException e) {
        String title = _i18nDefaultTitle;
        String msg = "";
        ConfigErrorDialog dialog = null;
        Frame frame = getParentFrame(parent);

        BaseConfigPanel.clearProgressIndicator(parent);

        if (e instanceof ValidationException) {
            title = _i18nValidationError;
            msg = e.getMessage();
            //dialog = new ConfigErrorDialog(parent, title, e);
        } else if (e instanceof RemoteRequestException) {
            RemoteRequestException ex = (RemoteRequestException) e;
            title = _i18nRemoteRequestError;

            if (ex.getType() == RemoteRequestException.LDAP) {
                msg = _i18nDN + ex.getDN() + "\n" + _i18nMatchedDN +
                        ex.getMatchedDN() + "\n\n" + ex.getMessage();
            } else {
                String url = ex.getURL();
                if (url.indexOf("?") > 0) {
                    url = url.substring(0, url.indexOf("?"));
                }
                msg = _i18nURL + url + "\n\n" + ex.getMessage();
            }

        }

        ActionMonitorPanel actionPanel =
                ActionMonitorPanel.getActiveInstance();
        if (actionPanel != null)
            actionPanel.setInterruptEnabled(false);

        if (Thread.currentThread().getClass().getName().equals("java.awt.EventDispatchThread")) {
            SuiOptionPane.showMessageDialog(frame, msg, title,
                    SuiOptionPane.ERROR_MESSAGE);
        } else {
            final String title_f = title, msg_f = msg;
            final Frame frame_f = frame;

            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                SuiOptionPane.showMessageDialog(
                                        frame_f, msg_f, title_f,
                                        SuiOptionPane.ERROR_MESSAGE);
                            }
                        }
                        );
            } catch (Exception e1) {
                Debug.println("showDialog invokeAndWait " + e1);
            }
        }


        if (actionPanel != null)
            actionPanel.setInterruptEnabled(true);
    }


    // In case NULL of not visible components are passed as a parent to showDialog(),
    // we create an helper Window to use as a parent.
    private static Window _helperWindow;


    // Return parent Frame. If one does not exist, create a helper Window.
    // We use a Window insead of Frame bacause it can be undetecable on the screen
    // by setting its size to [0,0]. Frame always has a visible title bar.
    public static Frame getParentFrame(Component c) {
        Frame f = SuiOptionPane.getFrameForComponent(c);

        if (f == null)
            f = UtilConsoleGlobals.getActivatedFrame();

        if (f == null) {
            if (_helperWindow == null) {
                _helperWindow = new Window (new Frame());
            }
            return (Frame)_helperWindow.getParent();
        } else {
            return f;
        }
    }




}
