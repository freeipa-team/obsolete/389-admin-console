/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.config.*;
import com.netscape.management.client.util.*;

/**
  *
  * @version 0.1 11/28/97
  * @author miodrag@netscape.com
  */
public class CGILoggingSetup extends CGIDataModel {
    private static String _taskURL = "admin-serv/tasks/Configuration/ServerSetup";

    public CGILoggingSetup(ConsoleInfo consoleInfo) {
        super(consoleInfo, _taskURL);
    }

    /**
      * CGI arguments used in getConfiguration()
     */
    public String getCGIParamsForGetOp() {
        return "op=get&" + AttrNames.CONFIG_ACCESSLOG + "=&" +
                AttrNames.CONFIG_ERRORLOG + "=";
    }

    /**
      * CGI arguments used in setConfiguration()
     */
    public String getCGIParamsForSetOp() {
        return "op=force_set&" + toURLformat(_data);
    }

    public void save() throws RemoteRequestException {
        super.save();
        ResourceSet resource = new ResourceSet("com.netscape.management.admserv.panel.panel");
        String msg = resource.getString("adminop", "NeedRestart");
        ConfigInfoDialog.showDialog(
                UtilConsoleGlobals.getActivatedFrame(), msg, "");
    }
}
