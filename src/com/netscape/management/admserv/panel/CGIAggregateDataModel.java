/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.util.*;
import java.net.*;
import com.netscape.management.client.util.*;
import com.netscape.management.admserv.config.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.panel.*;

/**
  *
  * @version 0.1 01/15/98
  * @author miodrag@netscape.com
  */


/**
 * A config data model that is an aggregate of multiple cgi data models.
 * It is intended for panels that need to work with multiple CGIs.
 */
public class CGIAggregateDataModel implements IConfigDataModel {
    private static String _clazz = "CGIAggregateDataModel";
    protected Hashtable _CGIs;
    protected String _modelName;
    protected ConsoleInfo _consoleInfo;

    public CGIAggregateDataModel(ConsoleInfo consoleInfo) {
        _CGIs = new Hashtable();
        _consoleInfo = consoleInfo;
    }

    public void add(String prefix, CGIDataModel cgi) {
        _CGIs.put(prefix, cgi);
    }

    public void remove(String prefix) {
        _CGIs.remove(prefix);
    }


    public void setDialogParent (java.awt.Component comp) {
        for (Enumeration e = _CGIs.elements(); e.hasMoreElements();) {
            CGIDataModel cgi = (CGIDataModel) e.nextElement();
            cgi.setDialogParent(comp);
        }
    }

    public IConfigDataModel getCGI(String prefix) {
        return (IConfigDataModel)_CGIs.get(prefix);
    }

    protected String getAttrPrefix (String attrName) {
        int dot = attrName.indexOf('.');
        if (dot > 0) {
            return attrName.substring(0, dot);
        } else {
            Debug.println("dot not found in " + attrName);
            return null;
        }
    }

    public String getAttribute(String attr) {
        //Debug.println("ATTR=" + attr + " PREFIX=" + getAttrPrefix(attr));
        IConfigDataModel cgi =
                (IConfigDataModel)_CGIs.get(getAttrPrefix(attr));
        if (cgi != null) {
            return cgi.getAttribute(attr);
        } else {
            Debug.println(_clazz +
                    ".getAttribute() CGI for the attr " + attr + " not in data model");
            return "";
        }
    }

    public void setAttribute(String attr,
            String val) throws ValidationException {
        IConfigDataModel cgi =
                (IConfigDataModel)_CGIs.get(getAttrPrefix(attr));
        if (cgi != null) {
            cgi.setAttribute(attr, val);
        } else {
            Debug.println(_clazz +
                    ".setAttribute() CGI for the attr " + attr + " not in data model");
        }
    }

    public Enumeration getAttributes() {
        // to do implement AggregateEnumerator
        return new CGIAggregateEnumerator(_CGIs);
    }

    public boolean isLoaded() {
        for (Enumeration e = _CGIs.elements(); e.hasMoreElements();) {
            IConfigDataModel cgi = (IConfigDataModel) e.nextElement();
            if (!cgi.isLoaded()) {
                return false;
            }
        }
        return true;
    }

    public boolean isModified() {
        for (Enumeration e = _CGIs.elements(); e.hasMoreElements();) {
            IConfigDataModel cgi = (IConfigDataModel) e.nextElement();
            if (cgi.isModified()) {
                return true;
            }
        }
        return false;
    }


    /**
      * Load all CGIs
     */
    public void load() throws RemoteRequestException {
        for (Enumeration e = _CGIs.elements(); e.hasMoreElements();) {
            IConfigDataModel cgi = (IConfigDataModel) e.nextElement();
            if (!cgi.isLoaded()) {
                cgi.load();
            }
        }
    }

    /**
      * Save all modified CGIs
     */
    public void save() throws RemoteRequestException {
        for (Enumeration e = _CGIs.elements(); e.hasMoreElements();) {
            IConfigDataModel cgi = (IConfigDataModel) e.nextElement();
            if (cgi.isModified()) {
                cgi.save();
            }
        }
    }

    public String getModelName() {
        return CGIDataModel.getServerName(_consoleInfo);
    }

    public ConsoleInfo getConsoleInfo() {
        return _consoleInfo;
    }

}


/**
  * An enumerator class for a hashtable of CGIDataModels
  */
class CGIAggregateEnumerator implements Enumeration {

    Hashtable _aggregateTable;
    Enumeration _aggregateEnum;
    Enumeration _curEnum;

    CGIAggregateEnumerator(Hashtable aggregateTable) {
        _aggregateTable = aggregateTable;
        _aggregateEnum = _aggregateTable.elements();
    }

    public boolean hasMoreElements() {
        if (_curEnum == null || !_curEnum.hasMoreElements()) {
            if (!_aggregateEnum.hasMoreElements()) {
                _curEnum = null;
                return false;
            }
            CGIDataModel curTable =
                    (CGIDataModel)_aggregateEnum.nextElement();
            _curEnum = curTable.getAttributes();
        }
        return _curEnum.hasMoreElements();
    }

    public Object nextElement() {
        if (_curEnum == null) {
            throw new NoSuchElementException("AggregateHashtableEnumerator");
        }
        return _curEnum.nextElement();
    }
}
