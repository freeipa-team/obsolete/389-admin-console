/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

public class AttrNames {

    public static final String CONFIG_CGI_PREFIX = "configuration";
    public static final String CONFIG_SERVERPORT = "configuration.nsserverport";
    public static final String CONFIG_SERVERADDRESS = "configuration.nsserveraddress";
    public static final String CONFIG_HOSTS = "configuration.nsadminaccesshosts";
    public static final String CONFIG_ADDRESSES = "configuration.nsadminaccessaddresses";
    public static final String CONFIG_USER = "configuration.nssuitespotuser";
    public static final String CONFIG_ACCESSLOG = "configuration.nsaccesslog";
    public static final String CONFIG_ERRORLOG = "configuration.nserrorlog";
    public static final String CONFIG_DSGW = "configuration.nsadminenabledsgw";
    public static final String CONFIG_ENDUSER = "configuration.nsadminenableenduser";

    public static final String ADMPW_CGI_PREFIX = "admpw";
    public static final String ADMPW_UID = "admpw.uid";
    public static final String ADMPW_PWD = "admpw.pw";

    public static final String DSCONFIG_CGI_PREFIX = "dsconfig";
    public static final String DSCONFIG_HOST = "dsconfig.host";
    public static final String DSCONFIG_PORT = "dsconfig.port";
    public static final String DSCONFIG_BASEDN = "dsconfig.basedn";
    public static final String DSCONFIG_SSL = "dsconfig.ssl";

    public static final String UGDSCONFIG_CGI_PREFIX = "ugdsconfig";
    public static final String UGDSCONFIG_INFOREF = "ugdsconfig.inforef";
    public static final String UGDSCONFIG_GLOBALDIRURL = "ugdsconfig.globaldirurl";
    public static final String UGDSCONFIG_DIRURL = "ugdsconfig.dirurl";
    public static final String UGDSCONFIG_BINDDN = "ugdsconfig.binddn";
    public static final String UGDSCONFIG_BINDPW = "ugdsconfig.bindpw";
    public static final String UGDSCONFIG_HOST = "ugdsconfig.host";
    public static final String UGDSCONFIG_PORT = "ugdsconfig.port";
    public static final String UGDSCONFIG_BASEDN = "ugdsconfig.basedn";
    public static final String UGDSCONFIG_SSL = "ugdsconfig.ssl";

    public static final String SNMP_CGI_PREFIX = "snmp";
    public static final String SNMP_COMMUNITIES = "snmp.communities";
    public static final String SNMP_MANAGERS = "snmp.managers";
}
