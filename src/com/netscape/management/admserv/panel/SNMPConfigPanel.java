/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.awt.*;
import javax.swing.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.config.*;

/**
  *
  * @version 0.1 11/28/97
  * @author miodrag@netscape.com
  */
public class SNMPConfigPanel extends TabbedConfigPanel {
    static ResourceSet _resource;
    static String _i18nStatusTab, _i18nCommunitiesTab, _i18nManagersTab;

    static {
        _resource = new ResourceSet("com.netscape.management.admserv.panel.panel");
        _i18nStatusTab = _resource.getString("status","Status");
        _i18nCommunitiesTab = _resource.getString("snmp","CommunitiesTab");
        _i18nManagersTab = _resource.getString("snmp","ManagersTab");
    }

    IConfigDataModel _configData;
    ConsoleInfo _consoleInfo;
    String _taskURL;
    String _username;
    String _password;

    SNMPStatusPanel _statusTab;
    SNMPCommunityPanel _communityTab;
    SNMPManagerPanel _managerTab;


    public SNMPConfigPanel(ConsoleInfo consoleInfo) {

        _consoleInfo = consoleInfo;
        _configData = new CGISNMPSetup(consoleInfo);

        addTabPanes();
    }

    public IConfigDataModel getDataModel() {
        return _configData;
    }

    public void setDataModel(IConfigDataModel data)
            throws ConfigPanelException {
        super.setDataModel(_configData = data);
    }

    protected void addTabPanes() {
        addTab(_i18nStatusTab, null,
                _statusTab = new SNMPStatusPanel("", _consoleInfo));


        addTab(_i18nCommunitiesTab, null,
                _communityTab = new SNMPCommunityPanel("", _configData));

        addTab(_i18nManagersTab, null,
                _managerTab = new SNMPManagerPanel("", _configData));
    }

    public void initialize() throws ConfigPanelException {

        try {
            if (! _configData.isLoaded()) {
                _configData.load();
            }
            super.initialize(); // initialize all
        } catch (ConfigPanelException e) {
            throw e;
        }
    }
}

