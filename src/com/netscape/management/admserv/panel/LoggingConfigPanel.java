/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.config.*;
import com.netscape.management.nmclf.SuiConstants;

/**
  *
  * @version 0.1 11/23/97
  * @author miodrag@netscape.com
  */
public class LoggingConfigPanel extends PluginConfigPanel {

    IConfigDataModel _configData;

    static ResourceSet _resource;
    static String _i18nLogPath, _i18nLogFile, _i18nErrorLogGroupbox,
    _i18nAccessLogGroupbox;

    static {
        _resource = new ResourceSet("com.netscape.management.admserv.panel.panel");
        _i18nLogPath = _resource.getString("logging","LogPath");
        _i18nLogFile = _resource.getString("logging","LogFile");
        _i18nAccessLogGroupbox = _resource.getString("logging","AccessLogGroupbox");
        _i18nErrorLogGroupbox = _resource.getString("logging","ErrorLogGroupbox");
    }

    Help _help;

    public static final int topInset =
            SuiConstants.SEPARATED_COMPONENT_SPACE;
    public static final int rghtColInset = SuiConstants.COMPONENT_SPACE;

    JLabel _lblAccessPath, _lblErrorPath;
    JTextField _txtAccessPath, _txtErrorPath;

    JLabel _lblAccessFile, _lblErrorFile;
    JTextField _txtAccessFile, _txtErrorFile;


    public LoggingConfigPanel(String title, ConsoleInfo consoleInfo) {
        super(title);
        _configData = new CGILoggingSetup(consoleInfo);

        setLayout(new BorderLayout());
        add(makeConfigPanel(), BorderLayout.NORTH);
        _help = new Help(_resource);
    }

    public void showHelp() {
        _help.contextHelp("loggingHelp");
    }

    public IConfigDataModel getDataModel() {
        return _configData;
    }

    public void setDataModel(IConfigDataModel data) {
        _configData = data;
        if (_configData.isLoaded()) {
            setPanelContent(_configData);
        }
    }

    public void initialize() throws ConfigPanelException {
        try {
            if (!_configData.isLoaded()) {
                _configData.load();
            }
            setPanelContent(_configData);
        } catch (ConfigPanelException e) {
            throw e;
        }
    }

    public void resetContent() {
        setPanelContent(_configData);
    }

    public void applyChanges() throws ValidationException {
        try {
            getPanelContent(_configData);
        } catch (ValidationException e) {
            throw e;
        }
    }

    public void registerEditComponents(EditMonitor editMonitor) {
        editMonitor.monitor(_txtAccessFile);
        editMonitor.monitor(_txtErrorFile);
    }

    public void setPanelContent(IConfigDataModel data) {
System.out.println("LoggingConfigPanel.setPanelContent: data: "+data.getClass().getName());
        String value;
        value = (String) data.getAttribute(AttrNames.CONFIG_ACCESSLOG);
        if (value == null) {
            _txtAccessPath.setText("");
            _txtAccessFile.setText("");
        } else {
            int i = value.lastIndexOf('/');
            _txtAccessPath.setText(value.substring(0, i));
            _txtAccessFile.setText(value.substring(i+1));
        }

        value = (String) data.getAttribute(AttrNames.CONFIG_ERRORLOG);
        if (value == null) {
            _txtErrorPath.setText("");
            _txtErrorFile.setText("");
        } else {
            int i = value.lastIndexOf('/');
            _txtErrorPath.setText(value.substring(0, i));
            _txtErrorFile.setText(value.substring(i+1));
        }
    }

    public void getPanelContent(IConfigDataModel data)
            throws ValidationException {
        try {
            data.setAttribute(AttrNames.CONFIG_ACCESSLOG,
                    _txtAccessPath.getText()+"/"+_txtAccessFile.getText());
            data.setAttribute(AttrNames.CONFIG_ERRORLOG,
                    _txtErrorPath.getText()+"/"+_txtErrorFile.getText());
        } catch (ValidationException e) {
            throw e;
        }
    }


    private JPanel makeConfigPanel() {

        JPanel p = new JPanel();
        GBC gbc = new GBC();
        GridBagLayout gbl;
        JPanel ctrlGroup;

        p.setLayout(gbl = new GridBagLayout());

        _lblAccessPath = new JLabel(_i18nLogPath);
        _txtAccessPath = new SingleByteTextField(16);
        _lblAccessFile = new JLabel(_i18nLogFile);
        _txtAccessFile = new SingleByteTextField(16); // 324429

        _txtAccessPath.setEditable(false);

        //gbc.setInsets(0,leftColInset,0,0);
        gbc.setGrid(0, 0, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, //anchor
                GBC.HORIZONTAL); //fill
        p.add(createLogGroup(_i18nAccessLogGroupbox,
                _lblAccessPath, _txtAccessPath,
                _lblAccessFile, _txtAccessFile), gbc);

        _lblErrorPath = new JLabel(_i18nLogPath);
        _txtErrorPath = new SingleByteTextField(16);
        _lblErrorFile = new JLabel(_i18nLogFile);
        _txtErrorFile = new SingleByteTextField(16); // 324429

        _txtErrorPath.setEditable(false);

        //gbc.setInsets(0,leftColInset,0,0);
        gbc.setGrid(0, 1, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, //anchor
                GBC.HORIZONTAL); //fill
        p.add(createLogGroup(_i18nErrorLogGroupbox,
                _lblErrorPath, _txtErrorPath,
                _lblErrorFile, _txtErrorFile), gbc);

        return p;
    }

    private JPanel createLogGroup(String groupTitle,
            JLabel lblPath, JTextField txtPath,
            JLabel lblFile, JTextField txtFile) {
        GridBagLayout gbl;
        GBC gbc = new GBC();
        JPanel group = new JPanel(gbl = new GridBagLayout());
        group.setBorder(BaseConfigPanel.createGroupBorder(groupTitle));

        lblPath.setFont(BaseConfigPanel.getLabelFont());
        gbc.setInsets(0, 0, 0, 0);
        gbc.setGrid(0, 0, 1, 1);
        gbc.setSpace(0.0, 0.0, GBC.WEST, //anchor
                GBC.NONE); //fill
        group.add(lblPath, gbc);

        gbc.setInsets(0, rghtColInset, 0, 0);
        gbc.setGrid(1, 0, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, // anchor
                GBC.HORIZONTAL); //fill
        group.add(txtPath, gbc);

        lblFile.setFont(BaseConfigPanel.getLabelFont());
        gbc.setInsets(0, 0, 0, 0);
        gbc.setGrid(0, 1, 1, 1);
        gbc.setSpace(0.0, 0.0, GBC.WEST, //anchor
                GBC.NONE); //fill
        group.add(lblFile, gbc);

        gbc.setInsets(0, rghtColInset, 0, 0);
        gbc.setGrid(1, 1, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, // anchor
                GBC.HORIZONTAL); //fill
        group.add(txtFile, gbc);

        return group;
    }
}
