/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.util.*;
import java.net.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.config.*;

/**
  *
  * @version 0.1 11/28/97
  * @author miodrag@netscape.com
  */
public class AdminConfigData extends CGIAggregateDataModel {
    public AdminConfigData(ConsoleInfo consoleInfo) {
        super(consoleInfo);
    }

    /**
         * Check if the server is NT platform
        */
    static public boolean isWindowsNTPlatform(ConsoleInfo ci) {
        String platform = (String) ci.getAdminOS();
        return (platform != null && platform.startsWith("Win"));
    }

    static public boolean isRunning(String adminURL) {
        try {
            URL serverURL = new URL(adminURL);
            URLConnection server = serverURL.openConnection();
            server.connect();
            return true;
        } catch (MalformedURLException ex) {
            Debug.println("ERROR AdminConfigData.isRunning: bad URL " +
                    adminURL);
        }
        catch (java.io.IOException ex) {
            Debug.println("AdminConfigData.isRunning: " + ex);
        }

        return false;
    }
}
