/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.config.*;
import com.netscape.management.nmclf.SuiConstants;

/**
  *
  * @version 0.1 11/23/97
  * @author miodrag@netscape.com
  */
public class DirectoryConfigPanel extends PluginConfigPanel implements SuiConstants {

    IConfigDataModel _configData;
    String _defaultPort = "389";
    String _defaultSSLPort = "636";

    static ResourceSet _resource;
    static String _i18nHost, _i18nPort, _i18nDSGroupbox, _i18nUseSSL;
    static String _i18nEnterLdapHost, _i18nEnterLdapPort, _i18nPortRange;
    static String _i18nWarnMigrate, _i18nWarnCert;

    static {
        _resource = new ResourceSet("com.netscape.management.admserv.panel.panel");
        _i18nDSGroupbox = _resource.getString("ldapds","DSGroupbox");
        _i18nHost = _resource.getString("ldapds","DSHost");
        _i18nPort = _resource.getString("ldapds","Port");
        _i18nUseSSL = _resource.getString("ldapds","UseSSL");
        _i18nEnterLdapHost = _resource.getString("ldapds","EnterLdapHost");
        _i18nEnterLdapPort = _resource.getString("ldapds","EnterLdapPort");
        _i18nPortRange = _resource.getString("common","PortRange");
        _i18nWarnMigrate = _resource.getString("ldapds","WarnMigrate");
        ;
        _i18nWarnCert = _resource.getString("ldapds","WarnCert");
        ;
    }

    Help _help;

    public static final int topInset = COMPONENT_SPACE,
    rghtColInset = DIFFERENT_COMPONENT_SPACE;

    JLabel _lblHost, _lblPort;
    JCheckBox _cbSSL;
    JTextField _txtPort;
    SingleByteTextField _txtHost;

    public DirectoryConfigPanel(String title, IConfigDataModel data) {
        super(title);
        _configData = (IConfigDataModel) data;

        setLayout(new BorderLayout());
        add(makeConfigPanel(), BorderLayout.NORTH);
        _help = new Help(_resource);
    }

    public void showHelp() {
        _help.contextHelp("ldapHelp");
    }

    public IConfigDataModel getDataModel() {
        return _configData;
    }

    public void setDataModel(IConfigDataModel data) {
        _configData = (IConfigDataModel) data;
        if (_configData.isLoaded()) {
            setPanelContent(_configData);
        }
    }

    public void initialize() throws RemoteRequestException {
        if (!_configData.isLoaded())
            _configData.load();
        setPanelContent(_configData);
        _cbSSL.addActionListener(_cbSSLListener);
    }

    public void resetContent() {
        setPanelContent(_configData);
    }

    public void applyChanges() throws ConfigPanelException {
        getPanelContent(_configData);
    }

    public void registerEditComponents(EditMonitor editMonitor) {
        editMonitor.monitor(_txtPort);
        editMonitor.monitor(_txtHost);
        editMonitor.monitor(_cbSSL);
    }

    public void setPanelContent(IConfigDataModel data) {
        setLDAPHost(data.getAttribute(AttrNames.DSCONFIG_HOST));
        setLDAPPort(data.getAttribute(AttrNames.DSCONFIG_PORT));
        setSSL(data.getAttribute(AttrNames.DSCONFIG_SSL));
    }

    ActionListener _cbSSLListener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (_cbSSL.isSelected() &&
                            _txtPort.getText().equals(_defaultPort)) {
                        _txtPort.setText(_defaultSSLPort);
                    } else if (!_cbSSL.isSelected() &&
                            _txtPort.getText().equals(_defaultSSLPort)) {
                        _txtPort.setText(_defaultPort);
                    }
                }
            };


    String getSSL() {
        return _cbSSL.isSelected() ? "true" : "false";
    }

    void setSSL(String booleanFlag) {
        _cbSSL.setSelected(booleanFlag != null && booleanFlag.equals("true"));
    }

    void setLDAPHost(String host) {
        _txtHost.setText(host == null ? "" : host);
    }

    void setLDAPPort(String port) {
        _txtPort.setText(port == null ? "" : port);
    }

    public void getPanelContent(IConfigDataModel data)
            throws ConfigPanelException {
        String host = _txtHost.getText().trim();
        String port = _txtPort.getText().trim();

        if (host.length() == 0) {
            throw new ValidationException("", _i18nEnterLdapHost);
        } else if (port.length() == 0) {
            throw new ValidationException("", _i18nEnterLdapPort);
        }

        int portNum = -1;

        try {
            portNum = Integer.parseInt(port);
            if (portNum < 1 || portNum > 65535) {
                throw new ValidationException("",_i18nPortRange);
            }
        } catch (Exception e) {
            throw new ValidationException("",_i18nPortRange);
        }

        // Try to connect to the DS to see if it is a valid one
        try {
            LDAPUtil.validateLDAPParams(host, portNum,
                    _cbSSL.isSelected(), null, null, null);
        } catch (IllegalArgumentException e) {
            throw new ValidationException("", e.getMessage());
        }

        data.setAttribute(AttrNames.DSCONFIG_HOST, host);
        data.setAttribute(AttrNames.DSCONFIG_PORT, port);
        data.setAttribute(AttrNames.DSCONFIG_SSL, getSSL());

        if (data instanceof AdminConfigData) {
            ((AdminConfigData) data).setDialogParent(this); // For ack dialog
        }
    }

    /**
      * Override BaseEditPanel.makeConfigPanel() to provide a custom panel,
      * called by baseEditPanel constructor
     */
    protected JComponent makeConfigPanel() {
        JPanel p = new JPanel();
        GBC gbc = new GBC();
        GridBagLayout gbl;
        JPanel ctrlGroup;

        p.setLayout(gbl = new GridBagLayout());

        //gbc.setInsets(topInset,leftColInset,0,0);
        gbc.setGrid(0, 0, 1, 1);
        gbc.setSpace(1.0, 0.0, GridBagConstraints.WEST, //anchor
                GridBagConstraints.HORIZONTAL); //fill
        p.add(createServerGroup(), gbc);

        return p;
    }

    private JPanel createServerGroup() {
        GridBagLayout gbl;
        GBC gbc = new GBC();
        JPanel group = new JPanel(gbl = new GridBagLayout());
        group.setBorder(
                BaseConfigPanel.createGroupBorder(_i18nDSGroupbox));
        int row = 0;

        gbc.setInsets(0, 0, 0, 0);
        gbc.setGrid(0, row, 3, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, // anchor
                GBC.HORIZONTAL); //fill
        group.add(createInfoMessage(), gbc);
        row++;

        _lblHost = new JLabel(_i18nHost);
        gbc.setInsets(0, 0, 0, 0);
        gbc.setGrid(0, row, 1, 1);
        gbc.setSpace(0.0, 0.0, GBC.EAST, // anchor
                GBC.NONE); //fill
        group.add(_lblHost, gbc);

        _txtHost = new SingleByteTextField(16);
        gbc.setInsets(0, rghtColInset, 0, 0);
        gbc.setGrid(1, row, 2, 1);
        gbc.setSpace(0.0, 0.0, GBC.WEST, // anchor
                GBC.HORIZONTAL); //fill
        group.add(_txtHost, gbc);
        _lblHost.setLabelFor(_txtHost);
        row++;

        _lblPort = new JLabel(_i18nPort);
        gbc.setInsets(topInset, 0, 0, 0);
        gbc.setGrid(0, row, 1, 1);
        gbc.setSpace(0.0, 0.0, GBC.EAST, //anchor
                GBC.NONE); //fill
        group.add(_lblPort, gbc);

        _txtPort = new JTextField("389",6);
        _txtPort.setDocument(FilteredInputDocument.allowDigitsOnly());
        gbc.setInsets(topInset, rghtColInset, 0, 0);
        gbc.setGrid(1, row, 1, 1);
        gbc.setSpace(0.0, 0.0, GBC.WEST, // anchor
                GBC.HORIZONTAL); //fill
        group.add(_txtPort, gbc);
        _lblPort.setLabelFor(_txtPort);

        _cbSSL = new JCheckBox(_i18nUseSSL, false);
        gbc.setInsets(topInset, rghtColInset * 2, 0, 0);
        gbc.setGrid(2, row, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, //anchor
                GBC.NONE); //fill
        group.add(_cbSSL, gbc);

        return group;
    }

    private JPanel createInfoMessage() {
        GridBagLayout gbl;
        GBC gbc = new GBC();
        JPanel group = new JPanel(gbl = new GridBagLayout());
        int row = 0;
        MultilineLabel mll;
        JLabel refLabel = new JLabel(); // unsed only for font

        gbc.setInsets(0, COMPONENT_SPACE, 0, 0);
        gbc.setGrid(1, row, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, //anchor
                GBC.HORIZONTAL); //fill
        group.add(mll = new MultilineLabel(_i18nWarnMigrate, 1, 36), gbc);
        mll.setFont(refLabel.getFont());
        row++;

        gbc.setInsets(COMPONENT_SPACE, COMPONENT_SPACE,
                DIFFERENT_COMPONENT_SPACE, 0);
        gbc.setGrid(1, row, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, //anchor
                GBC.HORIZONTAL); //fill
        group.add(mll = new MultilineLabel(_i18nWarnCert, 1, 36), gbc);
        mll.setFont(refLabel.getFont());
        row++;

        return group;
    }


    public void setPort(String port) {
        _txtPort.setText((port != null) ? port : "");
    }

    public String getPort() {
        String port = _txtPort.getText();
        return port;
    }

    public void setHost(String host) {
        _txtHost.setText((host != null) ? host : "");
    }

    public String getHost() {
        String host = _txtHost.getText();
        return host;
    }
}

