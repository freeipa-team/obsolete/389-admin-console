/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.util.*;
import java.awt.*;
import java.net.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.config.*;

/**
  *
  * @version 0.1 02/10/98
  * @author miodrag@netscape.com
  */
public class SNMPStatusPanel extends PluginConfigPanel {

    ConsoleInfo _target, _consoleInfo;
    Hashtable _statusTable = new Hashtable();

    static ResourceSet _resource;
    static String _i18nAgentOn, _i18nAgentOff, _i18nAgentUnknown;
    static String _i18nOn, _i18nOff, _i18nUnknown, _i18nMasterAgent,
    _i18nStatus;
    static String _i18nCheckStatusButton, _i18nRestartButton,
    _i18nStopButton;
    static String _i18nDialogTitleStop, _i18nDialogTitleRestart,
    _i18nDialogTitleStatus;
    static String _i18nCheckStatusToolTip, _i18nRestartToolTip, _i18nStopToolTip;

    static {
        _resource = new ResourceSet("com.netscape.management.admserv.panel.panel");
        _i18nAgentOn = _resource.getString("status","AgentOn");
        _i18nAgentOff = _resource.getString("status","AgentOff");
        _i18nAgentUnknown = _resource.getString("status","AgentUnknown");
        _i18nOn = _resource.getString("status","On");
        _i18nOff = _resource.getString("status","Off");
        _i18nUnknown = _resource.getString("status","Unknown");
        _i18nMasterAgent = _resource.getString("status","MasterAgent");
        _i18nStatus = _resource.getString("status","Status");
        _i18nCheckStatusButton = _resource.getString("status","CheckStatus");
        _i18nRestartButton = _resource.getString("status","Restart");
        _i18nStopButton = _resource.getString("status","Stop");
        _i18nDialogTitleStop = _resource.getString("status","DialogStopAgent");
        _i18nDialogTitleRestart = _resource.getString("status","DialogRestartAgent");
        _i18nDialogTitleStatus = _resource.getString("status","DialogStatusAgent");
        _i18nCheckStatusToolTip = _resource.getString("status","CheckStatusToolTip");
        _i18nRestartToolTip = _resource.getString("status","RestartToolTip");
        _i18nStopToolTip = _resource.getString("status","StopToolTip");
    }

    static Help _help;

    static String _cmdStatus = "status", _cmdRestart = "restart",
    _cmdStop = "stop";

    // Panel Controls
    JLabel _lblStatus;
    ButtonBar _buttons;
    String _agentColumn;

    public SNMPStatusPanel(String title, ConsoleInfo consoleInfo) {
        super(title);
        _target = _consoleInfo = consoleInfo;

        setLayout(new BorderLayout());
        add(makePanel(), BorderLayout.NORTH);
        _help = new Help(_resource);

        updateStatus();

    }

    public void showHelp() {
        _help.contextHelp("SNMPStatusHelp");
    }


    void updateStatus() {
        _lblStatus.setVisible(true);
        validate();
        String agent = (String)_target.get("SERVER_NAME");
        String status = (String)_statusTable.get(agent);
        status = (status == null) ? _i18nUnknown : status;
        if (status.equals(_i18nOn)) {
            _lblStatus.setText(_i18nAgentOn);
        } else if (status.equals(_i18nOff)) {
            _lblStatus.setText(_i18nAgentOff);
        } else {
            _lblStatus.setText(_i18nAgentUnknown);
        }
    }

    void updateStatus(String agent) {
        String status = (String)_statusTable.get(agent);
        status = (status == null) ? _i18nUnknown : status;

        if (status.equals(_i18nOn)) {
            _lblStatus.setText(_i18nAgentOn);
        } else if (status.equals(_i18nOff)) {
            _lblStatus.setText(_i18nAgentOff);
        } else {
            _lblStatus.setText(_i18nAgentUnknown);
        }

    }

    public void initialize() throws ConfigPanelException {
        checkStatus(_consoleInfo, true);
    }

    public IConfigDataModel getDataModel() {
        return null;
    }

    public void setDataModel(IConfigDataModel data)
            throws ValidationException {
        if (data != null) {
            if (data instanceof CGISNMPSetup) {
                _target = ((CGISNMPSetup) data).getConsoleInfo();
            } else {
                _target = _consoleInfo;
            }
            updateStatus();
        }
    }

    public void resetContent() throws ConfigPanelException {
    }

    public void applyChanges() throws ValidationException {
    }

    public void registerEditComponents(EditMonitor editMonitor) {
    }

    /**
      *
     */
    protected JComponent makePanel() {
        JPanel p = new JPanel();
        GBC gbc = new GBC();
        GridBagLayout gbl;
        JPanel ctrlGroup;
        //int row=0;

        p.setLayout(gbl = new GridBagLayout());

        _lblStatus = new JLabel(_i18nAgentUnknown);
        gbc.setInsets(10, 0, 10, 0);
        gbc.setGrid(0, 0, 2, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, //anchor
                GBC.HORIZONTAL); //fill
        p.add(_lblStatus, gbc);

        gbc.setInsets(10, 0, 10, 0);
        gbc.setGrid(0, 1, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, //anchor
                GBC.HORIZONTAL); //fill
        p.add(new JLabel(), gbc); // spacer

        // buttons
        _buttons = new ButtonBar(new String[]{_cmdStatus, _cmdRestart,
                                              _cmdStop}, 
                                 new String[]{_i18nCheckStatusButton,
                                              _i18nRestartButton, _i18nStopButton}, 
                                 new String[]{_i18nCheckStatusToolTip,
                                              _i18nRestartToolTip, _i18nStopToolTip},
                                 false);
        _buttons.addActionListener(_btnListener);
        _buttons.setBorder(new EmptyBorder(0, 10, 0, 0));

        gbc.setInsets(10, 0, 10, 0);
        gbc.setGrid(1, 1, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.NORTHEAST, //anchor
                GBC.HORIZONTAL); //fill

        p.add(_buttons, gbc);

        return p;
    }


    class StatusCheckListener implements IAdminOperationListener {
        AdminOperation _op;

        public StatusCheckListener(AdminOperation op) {
            _op = op;
        }

        public void cgiRequestCompleted(ConsoleInfo ci) {
            String tabStatus = _i18nUnknown;
            if (_op.getResultStatus() == 0) {
                Hashtable res = _op.getResultData();
                //Debug.printHashtable("snmp status res", res);
                String statusAttr = (String) res.get("status");
                if (statusAttr == null) {
                    Debug.println("status attr missing");
                } else if (statusAttr.equalsIgnoreCase("ON")) {
                    tabStatus = _i18nOn;
                } else {
                    tabStatus = _i18nOff;
                }
            }

            String agent = (String) ci.get("SERVER_NAME");
            _statusTable.put(agent, tabStatus);
            updateStatus(agent);
        }
    }

    class StopStartListener implements IAdminOperationListener {
        AdminOperation _op;
        boolean _stop;

        public StopStartListener(AdminOperation op, boolean stop) {
            _op = op;
            _stop = stop;
        }

        public void cgiRequestCompleted(ConsoleInfo ci) {
            String tabStatus = _i18nUnknown;
            if (_op.getResultStatus() == 0) {
                tabStatus = _stop ? _i18nOff : _i18nOn;

            }
            String agent = (String) ci.get("SERVER_NAME");
            _statusTable.put(agent, tabStatus);
            updateStatus(agent);
        }
    }

    protected void checkStatus(ConsoleInfo ci, boolean init) {
        String taskURL = "admin-serv/tasks/Operation/SNMPControl?ACTION=STATUS";
        AdminOperation operation =
                new AdminOperation(ci, taskURL);
        StatusCheckListener listener = new StatusCheckListener(operation);
        operation.monitorOperation(_i18nDialogTitleStatus + " ...",
                listener);

        if (init || operation.getDialog() != null ||
                operation.getFrame() != null)
            return;

        String title = _i18nDialogTitleStatus;
        DialogFrame dialog =
                new DialogFrame(this, title, operation.getPanel());
        dialog.setVisible(true);
        dialog.dispose();
        ModalDialogUtil.sleep();
    }

    protected void restart(ConsoleInfo ci) {
        String taskURL = "admin-serv/tasks/Operation/SNMPControl?ACTION=RESTART";
        AdminOperation operation =
                new AdminOperation(ci, taskURL);
        StopStartListener listener =
                new StopStartListener(operation, false);
        operation.monitorOperation("", listener);
        String title = _i18nDialogTitleRestart;
        DialogFrame dialog =
                new DialogFrame(this, title, operation.getPanel());
        dialog.setVisible(true);
        dialog.dispose();
        ModalDialogUtil.sleep();
    }

    protected void stop(ConsoleInfo ci) {
        String taskURL = "admin-serv/tasks/Operation/SNMPControl?ACTION=STOP";
        AdminOperation operation =
                new AdminOperation(ci, taskURL);
        StopStartListener listener = new StopStartListener(operation, true);
        operation.monitorOperation("", listener);
        String title = _i18nDialogTitleStop;
        DialogFrame dialog =
                new DialogFrame(this, title, operation.getPanel());
        dialog.setVisible(true);
        dialog.dispose();
        ModalDialogUtil.sleep();
    }

    ActionListener _btnListener = new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    String cmd = e.getActionCommand();
                    if (cmd.equals(_cmdStatus)) {
                        checkStatus(_target, false);
                    } else if (cmd.equals(_cmdRestart)) {
                        restart(_target);
                    } else if (cmd.equals(_cmdStop)) {
                        stop(_target);
                    } else {
                        Debug.println(
                                "SNMPStatusPanel: Unknown acction command " +
                                cmd);
                    }
                }
            };
}

