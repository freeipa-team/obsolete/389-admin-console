/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import com.netscape.management.client.util.*;
import com.netscape.management.admserv.config.*;

/**
  * @version 0.1 12/15/97
  * @author miodrag@netscape.com
  */
public class DialogFrame extends AbstractDialog {

    private Component _parent;
    public DialogFrame(Component parent, String title, Component comp) {
        super(ConfigErrorDialog.getParentFrame(parent), title,
                /*modal=*/true);
        _parent = parent;
        setPanel((JPanel) comp);
        pack();
        setMinimumSize(getSize());
        addWindowListener(_winListener);
    }

    /*public void setVisible(boolean show) {
           if (show) {
               ConfigErrorDialog.setDialogLocation(this, _parent);
           }
           super.setVisible(show);
       }*/

    private WindowListener _winListener = new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    DialogFrame.this.setVisible(false);
                }
            };

    private boolean _busyCursorOn;

    /**
     * Override setCursor to show busy cursor correctly
     */
    public void setCursor(Cursor cursor) {
        if (_busyCursorOn && cursor.getType() != Cursor.WAIT_CURSOR) {
            Debug.println(9, "DialogFrame.setCursor(): Discarding change of cursor");
            return;
        }
        super.setCursor(cursor);
    }

    /**
     * Force the cursor for the whole frame to be busy.
     * See how _busyCursorOn flag is used inside setCursor
     */
    public void setBusyCursor(boolean isBusy) {
        _busyCursorOn = isBusy;
        super.setCursor( Cursor.getPredefinedCursor(isBusy ?
                Cursor.WAIT_CURSOR : Cursor.DEFAULT_CURSOR));
    }
}
