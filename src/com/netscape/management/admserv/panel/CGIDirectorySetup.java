/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.util.*;
import java.net.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.config.*;

/**
  *
  * @version 0.1 11/28/97
  * @author miodrag@netscape.com
  */
public class CGIDirectorySetup extends CGIDataModel {
    private static String _taskURL = "admin-serv/tasks/Configuration/DirectorySetup";

    public CGIDirectorySetup(ConsoleInfo consoleInfo) {
        super(consoleInfo, _taskURL);
    }

    /**
      * CGI arguments used in getConfiguration()
     */
    public String getCGIParamsForGetOp() {
        return "op=getconfig";
    }

    /**
      * CGI arguments used in setConfiguration()
     */

    public String getCGIParamsForSetOp() {
        String sie = (String)_consoleInfo.get("SIE");
        return "op=setconfig&" + toURLformat(_data) +
                "&dsconfig.alias=" + URLByteEncoder.encodeUTF8(sie);
    }

    public void save() throws RemoteRequestException {

        super.save();

        ResourceSet resource = new ResourceSet("com.netscape.management.admserv.panel.panel");
        String msg1 = resource.getString("adminop", "ldapds-Changed");
        String msg2 = resource.getString("adminop", "ldapds-ChangedInfo");
        String title = resource.getString("adminop", "ldapds-DSGroupbox");
        ConfigInfoDialog.showDialog(getDialogParent(),
                msg1 + "\n\n" + msg2, title);
    }


}
