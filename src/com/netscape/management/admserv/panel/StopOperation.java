/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.net.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.config.*;
import com.netscape.management.client.comm.CommRecord;
import com.netscape.management.client.comm.CommManager;
import java.io.ByteArrayInputStream;

public class StopOperation extends AdminOperation {
    String _i18nSendRequest = _resource.getString("stop","SendRequest");
    String _i18nAccepted = _resource.getString("stop","Accepted");
    String _i18nWait = _resource.getString("stop","Wait");
    String _i18nDone = _resource.getString("stop","Done");

    boolean _serverRunning = true;

    public StopOperation(ConsoleInfo consoleInfo) {
        super(consoleInfo, "admin-serv/tasks/Operation/Stop");
        createActionPanel(/*multiLined=*/true);
    }


    /**
      * Issue stop request and record pid returned in the response. Afterwards, poll
      * for pid until new pid is detected
      */
    public void performOperationalTask(ConsoleInfo ci,
            String taskURL) throws RemoteRequestException {
        AdmTask task = null;
        _serverRunning = true;
        _actionPanel.setStatusText(_i18nSendRequest);
        super.performOperationalTask(ci, taskURL);
        _actionPanel.setStatusText(_i18nAccepted);

        long t0 = System.currentTimeMillis();

        _actionPanel.setStatusText(_i18nWait);
        _actionPanel.setStatusTextSameLIne("\n");

        // Poll for loss of connection to the server
        while (_serverRunning) {

            // Sleep 3 sec between consecutive poll requests
            try {
                Thread.currentThread().sleep(3000);
            } catch (Exception e) {}

            _serverRunning = AdminConfigData.isRunning(ci.getAdminURL());
            _actionPanel.setStatusTextSameLIne("*");

            // Start a new line if after 90 sec restart not detected
            if ((System.currentTimeMillis() - t0) > 90000) {
                t0 = System.currentTimeMillis();
                _actionPanel.setStatusTextSameLIne("\n");
            }
        }
        _actionPanel.setStatusText(_i18nDone + "\n");

        // Indicate that restart can not be done from the console any more
        IRestartControl restartControl =
                (IRestartControl) ci.get(IRestartControl.ID);
        if (restartControl != null) {
            restartControl.setServerShutdown(true);
        } else {
            Debug.println("StopOperation: restart activator not in ConsoleInfo");
        }

        Debug.println("StopOperation time = " +
                (System.currentTimeMillis() - t0) / 1000.);
    }
}

