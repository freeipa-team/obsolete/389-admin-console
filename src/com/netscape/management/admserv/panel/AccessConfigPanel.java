/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.awt.*;
import javax.swing.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.config.*;
import com.netscape.management.nmclf.SuiConstants;

/**
  *
  * @version 0.1 11/23/97
  * @author miodrag@netscape.com
  */
public class AccessConfigPanel extends PluginConfigPanel implements SuiConstants {

    IConfigDataModel _configData;

    static ResourceSet _resource;
    static String _i18nUID, _i18nPWD, _i18nConfirmPWD;
    static String _i18nAdmSuperuser, _i18nAllowDSGW;
    static String _i18nMsgEnterUID, _i18nMsgEnterPWD, _i18nMsgPWDMismatch;

    static {
        _resource = new ResourceSet("com.netscape.management.admserv.panel.panel");
        _i18nAdmSuperuser = _resource.getString("access","AdmSuperuser");
        _i18nUID = _resource.getString("access","UID");
        _i18nPWD = _resource.getString("access","PWD");
        _i18nConfirmPWD = _resource.getString("access","ConfirmPWD");
        _i18nAllowDSGW = _resource.getString("access","AllowDSGW");
        _i18nMsgEnterUID = _resource.getString("access","MsgEnterUID");
        _i18nMsgEnterPWD = _resource.getString("access","MsgEnterPWD");
        _i18nMsgPWDMismatch = _resource.getString("access","MsgPWDMismatch");
    }

    Help _help;

    JCheckBox _cbDSGW;
    JLabel _lblUserName;
    SingleBytePasswordField _txtPassword, _txtConfirm;


    public AccessConfigPanel(String title, IConfigDataModel data) {
        super(title);
        _configData = data;

        setLayout(new BorderLayout());
        add(makeConfigPanel(), BorderLayout.NORTH);
        _help = new Help(_resource);
    }


    public void showHelp() {
        _help.contextHelp("accessHelp");
    }

    public IConfigDataModel getDataModel() {
        return _configData;
    }

    public void setDataModel(IConfigDataModel data) {
        _configData = data;
        if (_configData.isLoaded()) {
            setPanelContent(_configData);
        }
    }

    public void initialize() throws RemoteRequestException {
        if (!_configData.isLoaded())
            _configData.load();
        setPanelContent(_configData);
    }

    public void resetContent() {
        setPanelContent(_configData);
    }

    public void applyChanges() throws ValidationException {
        try {
            getPanelContent(_configData);
        } catch (ValidationException e) {
            throw e;
        }
    }

    public void registerEditComponents(EditMonitor editMonitor) {
        editMonitor.monitor(_txtPassword);
        editMonitor.monitor(_txtConfirm);
        //editMonitor.monitor(_cbDSGW);
    }

    public void setPanelContent(IConfigDataModel data) {
        //String dsgw = data.getAttribute(AttrNames.CONFIG_DSGW);
        //setDSGW(!dsgw.equalsIgnoreCase("off"));

        setUserName(data.getAttribute(AttrNames.ADMPW_UID));
        _txtPassword.setText("");
        _txtConfirm.setText("");
    }

    public void getPanelContent(IConfigDataModel data)
            throws ValidationException {

        //String dsgw = (_cbDSGW.isSelected()) ? "on" : "off";
        //data.setAttribute(AttrNames.CONFIG_DSGW, dsgw);

        String newpw1 = _txtPassword.getText();
        String newpw2 = _txtConfirm.getText();
        String olduid = _configData.getAttribute(AttrNames.ADMPW_UID);

        // check if password changed
        if (newpw1.length() != 0) {
            if (!newpw1.equals(newpw2)) {
                throw new ValidationException("", _i18nMsgPWDMismatch);
            }
        }

        if (newpw1.length() > 0) {
            Debug.println("CHANGE PWD TO " + newpw1);
            _configData.setAttribute(AttrNames.ADMPW_PWD, newpw1);
        }

    }

    private void setUserName(String name) {
        _lblUserName.setText((name == null) ? "" : name);
        _lblUserName.repaint();
    }


    void setDSGW(boolean flag) {
        _cbDSGW.setSelected(flag);
    }

    private JPanel makeConfigPanel() {

        JPanel p = new JPanel();
        GBC gbc = new GBC();
        GridBagLayout gbl;
        JPanel ctrlGroup;

        p.setLayout(gbl = new GridBagLayout());

        //gbc.setInsets(0,leftColInset,0,0);
        gbc.setGrid(0, 0, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, //anchor
                GBC.HORIZONTAL); //fill
        p.add(createSuperuserGroup(), gbc);

        _cbDSGW = new JCheckBox(_i18nAllowDSGW, true);
        gbc.setInsets(COMPONENT_SPACE, 0, 0, 0);
        gbc.setGrid(0, 2, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, //anchor
                GBC.NONE); //fill

        //p.add(_cbDSGW, gbc);

        return p;
    }

    private JPanel createSuperuserGroup() {
        GridBagLayout gbl;
        GBC gbc = new GBC();
        JPanel group = new JPanel(gbl = new GridBagLayout());
        group.setBorder(
                BaseConfigPanel.createGroupBorder(_i18nAdmSuperuser));

        JLabel uidLabel = new JLabel(_i18nUID);
        gbc.setInsets(0, 0, 0, 0);
        gbc.setGrid(0, 0, 1, 1);
        gbc.setSpace(0.0, 0.0, GBC.EAST, //anchor
                GBC.NONE); //fill
        group.add(uidLabel, gbc);

        // Changed from textfield to a label whose contents is fetched by the CGI
        _lblUserName = new JLabel("."); // will be overridden
        gbc.setInsets(0, DIFFERENT_COMPONENT_SPACE, 0, 0);
        gbc.setGrid(1, 0, 1, 1);
        gbc.setSpace(0.0, 0.0, GBC.WEST, // anchor
                GBC.HORIZONTAL); //fill
        group.add(_lblUserName, gbc);
        uidLabel.setLabelFor(_lblUserName);

        JLabel pwdLabel = new JLabel(_i18nPWD);
        gbc.setInsets(COMPONENT_SPACE, 0, 0, 0);
        gbc.setGrid(0, 1, 1, 1);
        gbc.setSpace(0.0, 0.0, GBC.EAST, //anchor
                GBC.NONE); //fill
        group.add(pwdLabel, gbc);

        _txtPassword = new SingleBytePasswordField(16);
        gbc.setInsets(COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE, 0, 0);
        gbc.setGrid(1, 1, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, // anchor
                GBC.HORIZONTAL); //fill
        group.add(_txtPassword, gbc);
        pwdLabel.setLabelFor(_txtPassword);

        JLabel pwd2Label = new JLabel(_i18nConfirmPWD);
        gbc.setInsets(DIFFERENT_COMPONENT_SPACE, 0, 0, 0);
        gbc.setGrid(0, 2, 1, 1);
        gbc.setSpace(0.0, 0.0, GBC.EAST, //anchor
                GBC.NONE); //fill
        group.add(pwd2Label, gbc);

        _txtConfirm = new SingleBytePasswordField(16);
        gbc.setInsets(COMPONENT_SPACE, DIFFERENT_COMPONENT_SPACE, 0, 0);
        gbc.setGrid(1, 2, 1, 1);
        gbc.setSpace(1.0, 0.0, GBC.WEST, // anchor
                GBC.HORIZONTAL); //fill
        group.add(_txtConfirm, gbc);
        pwd2Label.setLabelFor(_txtConfirm);

        return group;
    }
}
