/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

/**
  * Interface used to control restart parameters for the Admin Server.
  *
  */

public interface IRestartControl {

    /**
      * Restart activator ID. To be used as a key when a restart activator is stored in ConsoleInfo
      * IRestartControl acivator = (IRestartControl) _consoleInfo.get(IRestartControl.ID);
      */
    public static final String ID = "RESTART_CONTROL";

    /**
      * Set the state of the "needsAutoRestart" flag
      * The Flag is used to indicate that the server must be restarted because of
      * configuration changes. The activator is supposed to restart the server when
      * it is clear that the user did not explicitly reqest a restart: this would usually
      * be the case when the server configurataion window is closed.
      *
    * @param flag true=needs restart, false=does not need restart
      */
    public void setNeedsAutoRestart(boolean flag);

    /**
      * Return the state of the "needsAutoRestart" flag
      *
      *  @returns "needsAutoRestart" flag for the server
      */
    public boolean needsAutoRestart();

    /**
      * Set the state of the "CanRestartFromConsole" flag
      * The Flag is used to indicate if the server can be restarted remotly from the console.
      * If false, the server can be restarted only locally from the command line
      *
    * @param flag true=can restart, false=can not need restart
      */
    public void setCanRestartFromConsole(boolean flag);

    /**
      * Return the state of the "CanRestartFromConsole" flag
      *
      *  @returns "CanRestartFromConsole" flag for the server
      */
    public boolean canRestartFromConsole();


    /**
      * Set the URL to be used after the restart. Some configuration changes, like change
      * or port or SSL setting, will change server URL. In such cases postRestartURL must be
      * set to the new URL
         *
         * @param url Server URL to be used after the restart
         */
    public void setPostRestartURL(String url);


    /**
      * Return the post restart url if set
      *
      *  @returns post restart URL or null
      */
    public String getPostRestartURL();


    /**
      * Set the state of the "ServerShutdown" flag
      * The Flag is used to indicate if the server has been shutdown. That means that no
      * CGI can be sent to the server
      *
    * @param flag true=can restart, false=can not need restart
      */
    public void setServerShutdown(boolean flag);

    /**
      * Return the state of the "ServerShutdown" flag
      *
      *  @returns "CanRestartFromConsole" flag for the server
      */
    public boolean isServerShutdown();



}
