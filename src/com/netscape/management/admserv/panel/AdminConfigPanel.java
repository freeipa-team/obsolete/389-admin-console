/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.awt.*;
import javax.swing.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.config.*;
import com.netscape.management.admserv.*;


/**
  *
  * @version 0.1 11/28/97
  * @author miodrag@netscape.com
  */
public class AdminConfigPanel extends TabbedConfigPanel {
    static ResourceSet _resource;
    static String _i18nNetworkTab, _i18nAccessTab,
    _i18nConfigDirectoryTab, _i18nUGDirectoryTab, _i18nEncryptionTab;
    ConsoleInfo _consoleInfo;

    static {
        _resource = new ResourceSet("com.netscape.management.admserv.panel.panel");
        _i18nNetworkTab = _resource.getString("config","NetworkTab");
        _i18nAccessTab = _resource.getString("config","AccessTab");
        _i18nConfigDirectoryTab = _resource.getString("config","ConfigDirectoryTab");
        _i18nUGDirectoryTab = _resource.getString("config","UGDirectoryTab");
	_i18nEncryptionTab = _resource.getString("config","EncryptionTab");
    }


    IConfigDataModel _configData;
    NetworkConfigPanel _networkTab;
    TurnOnSSL _encryptionTab;
    AccessConfigPanel _accessTab;
    DirectoryConfigPanel _configDirectoryTab;
    UGDirectoryConfigPanel _ugDirectoryTab;
    int _accessIdx = -1, _encryptionIdx = -1, _configDirectoryIdx = -1,
    _UGDirectoryIdx = -1;
    ;
    boolean _unixServer;

    public AdminConfigPanel(ConsoleInfo consoleInfo) {
        super(LAZY_CREATE);

        this._consoleInfo = consoleInfo;

        _unixServer = ! AdminConfigData.isWindowsNTPlatform(consoleInfo);
        _configData = new AdminConfigData(consoleInfo);
        addTabPanes();
    }

    public IConfigDataModel getDataModel() {
        return _configData;
    }

    public void setDataModel(IConfigDataModel data)
            throws ConfigPanelException {
        super.setDataModel(_configData = data);
    }

    protected void addTabPanes() {

        addDataModel(AttrNames.CONFIG_CGI_PREFIX,
                new CGIServerSetup(_consoleInfo));
        addTab(_i18nNetworkTab, null,
                _networkTab =
                new NetworkConfigPanel("", _configData, _unixServer));

        _accessIdx = lazyModeReserveTab(_i18nAccessTab, null);

        if (AdminFrameworkInitializer.canAccessSecurity) {
            _encryptionIdx = lazyModeReserveTab(_i18nEncryptionTab, null);
        }

        _configDirectoryIdx =
                lazyModeReserveTab(_i18nConfigDirectoryTab, null);
        _UGDirectoryIdx = lazyModeReserveTab(_i18nUGDirectoryTab, null);
    }

    public IPluginConfigPanel lazyModeCreateTab(int idx) {
        if (idx == _accessIdx) {
            addDataModel(AttrNames.ADMPW_CGI_PREFIX,
                    new CGIAccessSetup(_consoleInfo));
            return _accessTab = new AccessConfigPanel("", _configData);
        }
        else if (idx == _encryptionIdx) {
            return _encryptionTab = new TurnOnSSL(_consoleInfo);
        }
        else if (idx == _configDirectoryIdx) {
            addDataModel(AttrNames.DSCONFIG_CGI_PREFIX,
                    new CGIDirectorySetup(_consoleInfo));
            return _configDirectoryTab =
                    new DirectoryConfigPanel("", _configData);
        }
        else if (idx == _UGDirectoryIdx) {
            addDataModel(AttrNames.UGDSCONFIG_CGI_PREFIX,
                    new CGIUGDirectorySetup(_consoleInfo));
            return _ugDirectoryTab =
                    new UGDirectoryConfigPanel("", _configData);
        }
        else {
            throw new Error("Bad tab idx " + idx);
        }
    }

    private void addDataModel(String prefix, CGIDataModel cgi) {
        AdminConfigData aggrModel;

        aggrModel = (AdminConfigData)_configData;
        aggrModel.add(prefix, cgi);
    }

    public void initialize() throws ConfigPanelException {

        try {
            if (!_configData.isLoaded()) {
                _configData.load();
            }
            super.initialize(); // initialize all
        } catch (ConfigPanelException e) {
            throw e;
        }
    }
}

