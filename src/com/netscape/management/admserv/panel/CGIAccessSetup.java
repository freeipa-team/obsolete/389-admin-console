/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * END COPYRIGHT BLOCK **/
package com.netscape.management.admserv.panel;

import java.util.*;
import java.net.*;
import com.netscape.management.client.util.*;
import com.netscape.management.client.console.ConsoleInfo;
import com.netscape.management.admserv.config.*;

/**
  *
  * @version 0.1 11/28/97
  * @author miodrag@netscape.com
  */
public class CGIAccessSetup extends CGIDataModel {
    private static String _taskURL = "admin-serv/tasks/Configuration/AccessSetup";

    private boolean _dsgwAdded = false;

    public CGIAccessSetup(ConsoleInfo consoleInfo) {
        super(consoleInfo, _taskURL);
    }

    /**
      * CGI arguments used in getConfiguration()
     */
    public String getCGIParamsForGetOp() {
        return "op=get";
    }

    /**
      * CGI arguments used in setConfiguration()
     */
    // We may want to remove this since we won't be calling set with a new UID
    public String getCGIParamsForSetOp() {
        String uid = getAttribute(AttrNames.ADMPW_UID);
        return "op=set&" + AttrNames.ADMPW_UID + "=" +
                URLByteEncoder.encodeUTF8(uid);

    }

    public void save() throws RemoteRequestException {

        super.save();

        // Use change-sie-password command to change the password
        String pwd = getAttribute(AttrNames.ADMPW_PWD);

        if (pwd != null) {
            AdmTask task = null;
            String _taskURL = "admin-serv/commands/change-sie-password";
            String adminURL = _consoleInfo.getAdminURL() + _taskURL;
            adminURL = adminURL + "?" + URLByteEncoder.encodeUTF8(pwd);
            try {
                task = new AdmTask(new URL(adminURL),
                        _consoleInfo.getAuthenticationDN(),
                        _consoleInfo.getAuthenticationPassword());
            } catch (MalformedURLException e) {
                Debug.println("CGIDataModel.setConfiguration "+e);
                throw new RemoteRequestException(e);
            }

            if (0 == task.exec()) {
                // Since we've updated the Admin Password, 
                // let's update the one in _consoleInfo.
                // But, only do this if we are logged in as the admin user!
                String authDN = _consoleInfo.getAuthenticationDN().toLowerCase();
                String authUID = getAttribute(AttrNames.ADMPW_UID).toLowerCase();
                if (authDN.matches("uid=" + authUID + ", *ou=administrators, *ou=topologymanagement, *o=netscaperoot")) {
                    _consoleInfo.setAuthenticationPassword(pwd);
                }
            }

            int status = task.getStatus();
            Debug.println(adminURL + " "+status);
            AdminOperation.processAdmTaskStatus(adminURL, task,
                    _consoleInfo);
        }
    }
}
