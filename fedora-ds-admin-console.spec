%define major_version 1.1
%define minor_version 3

%define dsname fedora-ds
%define shortname fedora-admin
%define pkgname dirsrv

Name: fedora-ds-admin-console
Version: %{major_version}.%{minor_version}
Release: 1%{?dist}
Summary: Fedora Admin Server Management Console

Group: Applications/System
License: GPLv2
URL: http://directory.fedoraproject.org

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Source: http://directory.fedoraproject.org/sources/%{name}-%{version}.tar.bz2
Requires: %{dsname}-admin
BuildRequires: ant >= 1.6.2
BuildRequires: ldapjdk
BuildRequires: idm-console-framework
BuildRequires: java-devel >= 1:1.6.0
Obsoletes: fedora-admin-console

%description
A Java based remote management console used for Managing Fedora
Admin Server.

%prep
%setup -q
                                                                                
%build
%{ant} \
    -Dconsole.location=%{_javadir} \
    -Dbuilt.dir=`pwd`/built

%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT%{_datadir}/%{pkgname}/html/java
install -m644 built/package/%{shortname}* $RPM_BUILD_ROOT%{_datadir}/%{pkgname}/html/java
install -d $RPM_BUILD_ROOT%{_datadir}/%{pkgname}/manual/en/admin/help
install -m644 help/en/*.html $RPM_BUILD_ROOT%{_datadir}/%{pkgname}/manual/en/admin
install -m644 help/en/tokens.map $RPM_BUILD_ROOT%{_datadir}/%{pkgname}/manual/en/admin
install -m644 help/en/help/*.html $RPM_BUILD_ROOT%{_datadir}/%{pkgname}/manual/en/admin/help

# create symlinks
pushd $RPM_BUILD_ROOT%{_datadir}/%{pkgname}/html/java
ln -s %{shortname}-%{version}.jar %{shortname}-%{major_version}.jar
ln -s %{shortname}-%{version}.jar %{shortname}.jar
ln -s %{shortname}-%{version}_en.jar %{shortname}-%{major_version}_en.jar
ln -s %{shortname}-%{version}_en.jar %{shortname}_en.jar
popd

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE
%{_datadir}/%{pkgname}/html/java/%{shortname}-%{version}.jar
%{_datadir}/%{pkgname}/html/java/%{shortname}-%{major_version}.jar
%{_datadir}/%{pkgname}/html/java/%{shortname}.jar
%{_datadir}/%{pkgname}/html/java/%{shortname}-%{version}_en.jar
%{_datadir}/%{pkgname}/html/java/%{shortname}-%{major_version}_en.jar
%{_datadir}/%{pkgname}/html/java/%{shortname}_en.jar
%dir %{_datadir}/%{pkgname}/manual/en/admin
%{_datadir}/%{pkgname}/manual/en/admin/tokens.map
%doc %{_datadir}/%{pkgname}/manual/en/admin/*.html
%doc %{_datadir}/%{pkgname}/manual/en/admin/help/*.html

%changelog
* Tue Mar 31 2009 Rich Megginson <rmeggins@redhat.com> 1.1.3-1
- this is the 1.1.3 release

* Thu Jul  3 2008 Rich Megginson <rmeggins@redhat.com> 1.1.2-1
- disable SSLv2 settings

* Wed Jan 16 2008 Rich Megginson <rmeggins@redhat.com> 1.1.1-2
- rename package to fedora-ds-admin-console

* Thu Jan 10 2008 Rich Megginson <rmeggins@redhat.com> 1.1.1-1
- changes for fedora package review
- added requires for icedtea java
- added LICENSE

* Wed Dec 19 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-4
- This is for the Fedora DS 1.1 release

* Thu Oct 25 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-3
- updated sources - use dirsrv as package name

* Wed Aug  8 2007 Nathan Kinder <nkinder@redhat.com> 1.1.0-2
- Added online help files into package.

* Thu Aug  2 2007 Nathan Kinder <nkinder@redhat.com> 1.1.0-1
- Initial creation
